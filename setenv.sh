#!/bin/bash
ssh_key_name=$(cat ./config | grep 'ssh_key_name' | awk '{print $2}')
bucket_name=$(cat ./config | grep 'bucket_name' | awk '{print $2}')
region=$(cat ./config | grep 'region' | awk '{print $2}')
mgh_home=$(cat ./config | grep 'mgh_home' | awk '{print $2}')
aws_account_id=$(cat ./config | grep 'aws_account_id' | awk '{print $2}')
dbpass=$(cat ./config | grep 'dbpass' | awk '{print $2}')
dbuser=$(cat ./config | grep 'dbuser' | awk '{print $2}')
dbname=$(cat ./config | grep 'dbname' | awk '{print $2}')
sheet_name=$(cat ./config | grep 'sheet_name' | awk '{print $2}')
sheet_tab=$(cat ./config | grep 'sheet_tab' | awk '{print $2}')
credentials_path=$(cat ./config | grep 'credentials_path' | awk '{print $2}')
client_vpc_id=$(cat ./config | grep 'client_vpc_id' | awk '{print $2}')
tables=('cloudwatch_ec2' 'cloudwatch_loadbalancers' 'cloudwatch_rds' 'docker_containers' 'ec2' 'ec2_sg' 'iam_inst_profile' 'idmapping' 'inbound_traffic' 'inboundconnection' 'jenkins_job_builders' 'jenkins_job_parameters' 'jenkins_job_triggers' 'jenkins_jobs' 'jenkins_jobs_scm' 'lb_instances' 'lb_sg' 'lb_subnets' 'loadbalancers' 'networkinterface' 'open_ports' 'osinfo' 'outbound_traffic' 'outboundconnection' 'port_process_info' 'processes' 'rds' 'rds_subnets' 'route_53' 'routes' 's3' 's3_acl' 's3_objects' 'security_groups' 'sg_egress_rules' 'sg_ingress_rules' 'subnets' 'sysperformance' 'traffic_requests' 'traffic_routing' 'traffic_routing_filtered')


export AWS_DEFAULT_REGION=$region REGION=$region
export BUCKET_NAME=$bucket_name SSH_KEY_NAME=$ssh_key_name

# Database variables
export DB_USER=$dbuser DB_PASS=$dbpass DB_NAME=$dbname 
export BUCKET_NAME=$bucket_name SSH_KEY_NAME=$ssh_key_name

# Grab RDS endpoint
aws s3 cp s3://$bucket_name/statefiles/rds.tfstate .
endpoint=$(cat rds.tfstate | jq -r '.outputs.rds_endpoint.value')
rm rds.tfstate
export DB_HOST=$endpoint

# Google Credentials
export CREDENTIALS_PATH=$credentials_path

# Google Sheet details to add data
export sheet_name sheet_tab tables
export CLIENT_VPC_ID=$client_vpc_id
export MGH_HOME=$mgh_home AWS_ACCOUNT_ID=$aws_account_id

# Get project security group id from s3 bucket
aws s3 cp s3://$bucket_name/statefiles/discovery.tfstate .
discovery_sg=$(cat discovery.tfstate | jq -r '.outputs.discovery_allow_sg_id.value')
rm discovery.tfstate
export DISCOVERY_ID=$discovery_sg
