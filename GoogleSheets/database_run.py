"""Applies function to send DB data to Google Sheets with argparse env variables"""

# Requirements:
# Google Console Service Account created
# Google credentials file: credentials.json
# Google Drive API and Google Sheets API enabled for the Project
# Worksheet/s to store the RDS information, along with a tab for each table
# Activated a Python Virtual Environment
# Installed the required Python modules
# Run `. ../config.sh` to set necessary variables

# Modules:
# pip3 install gspread oauth2client gspread_formatting boto3
# pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

import os
import argparse

from database_function import updatesheet

# Set environment variables
parser = argparse.ArgumentParser(description='Gets information from mysql database and sends to a Google Worksheet. --help for more information.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-dt', '--dbtable', default=os.environ.get('dbtable'), help='The name of the table in the database to be accessed - set in "run_all.sh". Overrides value set in dbtable environment variable.')
parser.add_argument('-sn', '--sheet_name', default=os.environ.get('sheet_name'), help='The name of the Google Sheet that should be updated. Overrides value set in sheet_name environment variable.')
parser.add_argument('-st', '--sheet_tab', default=os.environ.get('sheet_tab'), help='The tab name within the Google Sheet to update. Overrides value set in sheet_tab environment variable.')
parser.add_argument('-cr', '--cr_path', default=os.environ.get('CREDENTIALS_PATH'), help='The path to the Google Credentials JSON file. Overrides value set in credentials_path environment variable.')
args = vars(parser.parse_args())

# The argument given relates to the database table
#dbtable = sys.argv[1]

# Apply function to pull from RDS database
updatesheet(args['sheet_name'], args['sheet_tab'], args['dbtable'], args['cr_path'], args['dbname'])
