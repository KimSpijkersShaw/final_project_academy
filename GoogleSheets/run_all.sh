#!/bin/bash
# Shell script that asks for user input for a database and runs database_run.py for the input table, or runs every table listed in the config.sh variable.
# Run from root directory

# Set config variables - including list of tables
#. ../config.sh
. ./setenv.sh
cd GoogleSheets/

# Re-format tables variable into bash array
#echo $tables | tr -s "\'\'" "\'\ \'"
#tables=$(echo $tables | tr -s "\'\'" "\' \'")
#tables=$(echo $tables | tr -s '[]' '()' | tr -s ',' ' ')
# Causing issues with the ${tables[@]} output

# User Input
# If a specific table is chosen, only that sheet will be updated

echo "Select whether to update all tables in Google Sheets, or to pick a specific table"
PS="Please choose an option for a table from the database, or choose 'ALL': "
echo $PS

q=$(( ${#tables[@]} + 2 ))

select opt in "ALL" "${tables[@]}" "Quit"
do
  case "$REPLY" in
  1 ) echo "You chose $opt, so each table will be updated in the Worksheet."
      echo "Iterating through all Tables to send to Google Sheets"
      for x in "${tables[@]}"
      do
        sleep 20 # API Limit on requests per minute
        export dbtable=$x
        python3 ./database_run.py
      done
      echo "Your Google Sheet is complete!"
      exit ;;
  $q ) echo "You chose to $opt. Goodbye!"
      break ;;
  * ) for item in "${tables[@]}"
      do
        if [ "$opt" == "$item" ]; then
          export dbtable=$opt
          echo "You chose the database table $opt."
          python3 ./database_run.py
          exit
        fi
      done
      echo "Invalid choice. Try another."
      continue ;;
  esac
done
