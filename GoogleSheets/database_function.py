"""Python Function to connect to the database and send data to Google Sheets"""

# Requirements:
# Google Console Service Account created
# Google credentials file: credentials.json
# Google Drive API and Google Sheets API enabled for the Project
# Worksheet/s to store the RDS information, along with a tab for each table
# Activated a Python Virtual Environment
# Installed the required Python modules
# Run `. ../config.sh` to set necessary variables

# Modules:
# pip3 install gspread oauth2client gspread_formatting boto3
# pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
import sys
import os

import datetime
from datetime import date, time
import time

import argparse

import mysql.connector
from mysql.connector import errorcode

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from gspread_formatting import *
#from colours import Colours

# Set environment variables for database connection
parser = argparse.ArgumentParser(description='Gets information from mysql database and sends to a Google Worksheet. --help for more information.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
args = vars(parser.parse_args())

# Input Arguments:
#   sheet_name: The name of the target spreadsheet to send the discovery data to
#   sheet_tab: The name of the target sheet within the target spreadsheet; usually the same as the name of the database table
#   dbtable: The name of the database table which should be sent to the sheet
#   credentials_path: Path to the Google Credentials JSON file
#   dbname: The name of the database containing discovery information

def updatesheet(sheet_name, sheet_tab, dbtable, credentials_path, dbname):
    """Retrieve data from the MySQL database and output to a Google Spreadsheet."""
    # Send to sheet of the same name as the DB Table, unless otherwise specified
    if sheet_tab == '':
        sheet_tab = dbtable

    print(sheet_name+'; '+sheet_tab)

    # Connect to the database
    try:
        connection = mysql.connector.connect(host=args['dbhost'],
                                    user=args['dbuser'],
                                    password=args['dbpass'],
                                    database=dbname)
    except:
        print("Couldn't connect to database")
        sys.exit(1)
    print("Database connection successful")
    cursor = connection.cursor()

    # Authorise Google Service Account Credentials
    google_data = gspread.service_account(filename=credentials_path)

    # Open target worksheet
    worksheet = google_data.open(sheet_name)

    # Create sheet tab if it doesnt yet exist
    try:
        worksheet.add_worksheet(title=sheet_tab, rows="200", cols="20")
    except:
        pass

    worksheet = google_data.open(sheet_name).worksheet(sheet_tab)
    worksheet.clear()

    # Add timestamp for update
    timestamp = time.strftime("%Y-%m-%d;%H:%M",time.localtime())
    worksheet.update('A1', 'Last Update: ' + timestamp)

    # Fetch column names
    cols = "DESCRIBE " + dbtable #How to extract column names from table
    cursor.execute(cols)
    desc = cursor.fetchall()
    connection.commit()
    count_cols = len(desc)
    col_names = []
    for i in range(count_cols):
        col_names.append(desc[i][0])
    # Input column names
    headings = 'A2:' + chr(ord('A')+count_cols) + '2'
    worksheet.update(headings, [col_names])

    # Find the total number of rows in the table
    rows = "SELECT COUNT(*) FROM `" + dbtable + "`"
    cursor.execute(rows)
    count_rows = cursor.fetchall()[0][0]
    connection.commit()

    # If the table is large, the API will need to pause between rows due to request limit
    # Creation of variable 'sleep' is no longer necessary due to the exception loop
    if 30 < count_rows < 198:
        sleep = 1
        print("This is a large table (under 200 rows): If an APIError occurs due to exceeding request limit, the API will pause before continuing")
    elif 198 <= count_rows <= 1500:
        sleep = 1
        print("This is a large table (under 1500 rows): If an APIError occurs due to exceeding request limit, the API will pause before continuing")
        worksheet.add_rows(count_rows - 198)
    elif count_rows >= 1500:
        sleep = 1.5
        worksheet.add_rows(count_rows - 198)
        print(f"This table is VERY large - {count_rows} rows. Note it could be a long time to send to a Sheet due to API request limit.")
        #x = input("Please confirm you want to continue by typing 'y': ")
        #if x != 'y':
        #    sys.exit(1)
    else:
        sleep = 0

    # Gathering row data from Database
    count = 0
    for i in range(count_rows):
        # sql = grabs a single row to be added
        sql = "SELECT * FROM `" + dbtable + "` LIMIT "+str(count)+",1"
        cursor.execute(sql)
        current_row = cursor.fetchall()[0]
        connection.commit()

        # Destination cells for the data
        ch = chr(ord('A') + len(col_names))
        list_cells = 'A' + str(count+3) + ':' + ch + str(count+3)

        # Update cell range with relevant data
        #worksheet.update(list_cells, [list(current_row)])

        # Sleep between rows if necessary
        #time.sleep(sleep)

        # Exception Loop for API Error
        exc_count = 0
        while True:
            try:
                worksheet.update(list_cells, [list(current_row)])
                break
            except gspread.exceptions.APIError:
                if exc_count == 0:
                    print("Waiting... API Request Limit Exceeded on row "+str(count))
                time.sleep(30)
                exc_count += 1
                continue

        count += 1

    # Formatting text and background colour
    time_cell = cellFormat(
    backgroundColor=color(0.9, 0.9, 0.9),
    textFormat=textFormat(bold=True, foregroundColor=color(0, 0, 0)),
    horizontalAlignment='LEFT'
    )

    titles_cells = cellFormat(
    backgroundColor=color(1, 1, 1), # blue (0.1, 0.9, 0.9)
    textFormat=textFormat(bold=True, foregroundColor=color(0, 0, 0)),
    horizontalAlignment='LEFT'
    )

    format_cell_ranges(worksheet, [('A1', time_cell), (headings, titles_cells)])

    # Formatting column width
    # This section should autosize the column widths, but is failing due to a TypeError
    # -- to do with the wrong gspread batch_update module
    body = {
        "requests": [
            {
                "autoResizeDimensions": {
                    "dimensions": {
                        "sheetId": worksheet._properties['sheetId'],
                        "dimension": "COLUMNS",
                        "startIndex": 0,
                        "endIndex": count_cols
                    }
                }
            }
        ]
    }

    try:
        worksheet.batch_update(body)
    except TypeError:
        print("Batch_update TypeError: Can't autosize columns")
        pass

    # Close database connection
    connection.close()
    cursor.close()
    
    end_timestamp = str(time.strftime("%Y-%m-%d;%H:%M",time.localtime()))
    print(f"As of {end_timestamp}, The tab {sheet_tab} within the Worksheet {sheet_name} is complete!\n")
