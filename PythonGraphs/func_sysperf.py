""" Function to plot from sysperformance google sheet """

import os, sys

import datetime
from datetime import date, time
import time

import gspread
from oauth2client.service_account import ServiceAccountCredentials

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def sysplot(df):

    # Close all figures initially
    plt.close('all')

    # Set Theme in Seaborn
    sns.set_theme(font_scale=1.25)

    # CONVERT COLUMNS TO TYPE
    df = df.astype({'timestamp': 'datetime64[ns, US/Eastern]'})
    for i in df.columns[3:]:
        try:
            df = df.astype({i: 'float64'})
        except ValueError:
            print(i + " can't be changed to float")


    # Plot all together
    #ax_all = sns.relplot(data=df) # Not very useful
    #ax_all.set(xlabel='Time', title='All System Performance Information from Agent')

    # Plot CPU Usage percentage
    #sns.relplot(data=df, kind = 'line', x='timestamp',y='total_cpu_usage_pct')
    ax_cpu = sns.relplot(data=df, kind = 'line', x='timestamp',y='total_cpu_usage_pct',hue="agent_id")
    ax_cpu.set(xlabel='Time', ylabel='Percentage of CPU Usage', title='CPU Usage from Agent over Discovery Time Period')

    # matplotlib plots
    #df.plot(x='timestamp', y=['total_disk_read_ops_per_sec','total_disk_write_ops_per_sec'], figsize=(10,5), grid=True)
    #df.plot(x='timestamp', y=['total_network_bytes_read_per_sec_in_kbps', 'total_network_bytes_written_per_sec_in_kbps','total_disk_bytes_read_per_sec_in_kbps','total_disk_bytes_written_per_sec_in_kbps'], figsize=(10,5), grid=True)
    #df.plot(x='timestamp', y=['total_network_bytes_written_per_sec_in_kbps','total_disk_write_ops_per_sec','total_disk_bytes_written_per_sec_in_kbps'], figsize=(10,5), grid=True)
    #df.plot(x='timestamp', y=['total_disk_write_ops_per_sec','total_disk_bytes_written_per_sec_in_kbps'], figsize=(10,5), grid=True)
    #df.plot(x='timestamp', y='total_disk_write_ops_per_sec', figsize=(10,5), grid=True)

    # All 3 write per sec with 2 axes
    ax_write = df.plot(x='timestamp', y=['total_disk_bytes_written_per_sec_in_kbps'], figsize=(10,5), grid=False, legend=False)
    ax_write.set(xlabel='Time', ylabel='kbps', title='Disk Bytes written over Discovery Time Period')
    ax_write2 = ax_write.twinx()
    df.plot(x='timestamp', y='total_disk_write_ops_per_sec', ax=ax_write2, legend=False, color="r")
    ax_write.figure.legend()

    return plt
