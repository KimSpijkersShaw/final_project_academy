""" Function to output a pandas dataframe of any Google Sheet """

import os
import argparse

import gspread
from oauth2client.service_account import ServiceAccountCredentials

import pandas as pd
import numpy as np

def getpandas(sheet_name, credentials_path, sheet_tab):

    # Authorises Google Service Account Credentials
    google_data = gspread.service_account(filename=credentials_path)

    # Open worksheet, collect all records and store as a dictionary
    worksheet = google_data.open(sheet_name).worksheet(sheet_tab)

    # Get all data from the sheet
    sheet_list = worksheet.get_all_values()

    # Remove 'Last Update' cell from first row, then get column headers from second row
    sheet_list.pop(0)
    headers = sheet_list.pop(0)

    # Read into DataFrame
    df = pd.DataFrame(sheet_list, columns=headers)

    return df
