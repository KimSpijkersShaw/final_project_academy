""" This script would run several functions to make plots from Sheets, then send to the report. Close each set of plots in turn for the next plots to be created. """

import os
import argparse

import datetime
from datetime import date, time
import time

import gspread
from oauth2client.service_account import ServiceAccountCredentials

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn

from func_getpandas import getpandas
from func_sysperf import sysplot
from func_trafficreq import traffic_reqplot

# Set environment variables
parser = argparse.ArgumentParser(description='Gets information from mysql database and sends to a Google Worksheet. --help for more information.')
parser.add_argument('-sn', '--sheet_name', default=os.environ.get('sheet_name'), help='The name of the Google Sheet that should be updated. Overrides value set in sheet_name environment variable.')
parser.add_argument('-cr', '--cr_path', default=os.environ.get('CREDENTIALS_PATH'), help='The path to the Google Credentials JSON file. Overrides value set in credentials_path environment variable.')
args = vars(parser.parse_args())

# System performance plots
df = getpandas(sheet_name, cr_path, 'sysperformance')
plt1 = sysplot(df)
plt1.show()

# Inbound Traffic plots
df = getpandas(sheet_name, cr_path, 'traffic_requests')
plt2 = traffic_reqplot(df)
plt2.show()

plt1.show()

# Send to Google Doc Report
