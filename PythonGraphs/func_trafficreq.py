""" Function to automate plot from traffic_requests from google sheet """

import os
import argparse

import datetime
from datetime import date, time
import time

import gspread
from oauth2client.service_account import ServiceAccountCredentials

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def traffic_reqplot(df):

    # Close all figures initially
    plt.close('all')

    # Set Theme in Seaborn
    sns.set_theme(font_scale=1.25)

    # CONVERT COLUMNS TO TYPE
    #df = df.astype({'timestamp': 'datetime64[ns, US/Eastern]'})
    for i in df.columns[3:]:
        try:
            df = df.astype({i: 'float64'})
        except ValueError:
            print(i + " can't be changed to float")

    ax = sns.catplot(data=df, kind="bar", x="machine_ip", y="no_of_total_requests")

    ax.set(xlabel='Machine IP', ylabel='Total Number of Requests', title='Total Traffic Requests on each machine')

    return plt
