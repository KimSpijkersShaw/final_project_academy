
# AL Academy Oct 2020 final project #

## Contents
- Pre-requisites
- Setup
    - Setting Variables
    - Project S3 Bucket
    - Discovery Machine
    - Discovery RDS
    - Google APIs and Sheets Setup
- Gathering Data
    - Logging on to the discovery machine
    - Accessing the Project Key
    - Setting the Environment
- Infrastructure Data Collection
    - Loadbalancer, RDS and S3 Buckets
    - Instances, Subnets, Security Groups, IAM Instance Profile Associations
- Traffic and Connectivity Data Collection
- OS Data Collection
- CloudWatch Data Collection
    - EC2 Metrics
    - Loadbalancer Metrics
    - RDS Metrics
- Discovery Agent
    - Installing Agent
    - Data collection from AWS Discovery Agent through Athena
- Machine Install Data Collection
    - Package Install Lists
    - Finding Running Programs
    - Docker Containers
    - Jenkins Jobs
- Retrieving Data
    - Google Sheets API
    - Automated Graphing
- Post Discovery
    - Destroying the RDS
    - Destroying the Discovery Machine
    - Destroying the S3 bucket


## Pre-requisites
It is neccessary that we have SSH access to the machines within the client's infrastructure. Therefore, the client would either need to give us all SSH keys for the infrastructure, or we would create a key and the client would then add this new key to all their machines.

In this case, client has deployed our key to all machines, so our key can be used to access all machines.

This would be carried out after the initial discovery scripts have been run so all machines have been found, but before analysis of the machines began e.g. traffic tests.

## Setup
### Setting Variables
The *config* file in the root directory of this repo is the place all the other modules will draw their variables from so it is important this is filled in correctly.

In general, it is a good idea to keep all of these variables lower case and use only hyphens (-) and underscores (_). Some variables cannot contain underscores.

The structure of this file is white space separated so no quotes are needed for most of the variables, each line should be:

variable_name variable value

The variables that should be added here are:

- AWS related variables
    - aws_account_id
    - region
    - mgh_home_region
    - image_id
    - bucket_name
    - client_vpc_id
    - client_pub_subnet
    - default_vpc_id
    - default_pub_subnet
    - default_subnets List of strings ( format [" "," "," "] can be any length)
    - ssh_key_name
- Project related variables
    - project_name (no underscores _)
    - home_ip List of strings ( format [" "," "," "] can be any length)
    - team
    - start_date
    - end_date
- Discovery database related variables
    - dbuser
    - dbpass

### Project S3 Bucket
The project S3 bucket will be used to store the project ssh key, the terraform state files for the discovery machine and RDS and a small amount of data which cannot be easily stored in mysql during the discovery process.

It can be created by running

    cd discovery_storage
    ./s3bucket.sh

### Discovery Machine
The discovery machine will be launched in the client's VPC, it is where all scripts that need to access the client's machines will be run from.

It can be launched with terraform by running

    cd discovery_machine
    ./launch_discovery.sh

This will create:

- The discovery machine itself, an amazon linux ec2 instance with several necessary programs installed.
- A discovery iam role for it that gives it full access to s3 buckets and read-only access to all of ec2, rds and route53.
- A project SSH key which will be saved into the project S3 bucket and downloaded locally on each machine that needs to use it, then deleted again, for security.
- Two security groups
    - The discovery_admin security group will be attached to the discovery machine, it will then have full access to any machine which has the discovery_allow security group.
    - The discovery_allow security group will automatically be added to the security group of all of the client's ec2 instances, so that they can be accessed.

The discovery machine is provisioned with ansible so that necessary programs and python modules are installed on it at launch.
To add any more necessary installs when extending the project, add ansible roles in the *discovery_machine/ansible_provison/roles* directory.

The ansible provision doesn't currently pip install all of the necessary python3 modules for some of the scripts, so this will need to be done manually the first time you log in to the discovery machine, see the Setting the Environment section in Gathering Data.

### Discovery RDS
The data found suring the discovery process will be saves on a designated amazon relational database server.

It will be launched in the default VPC to minimalise interference with client infrastructure and will only be accessible by the discovery machine and the home ip addresses give in the

This server can be created and set up with the necessary schema and security groups by running

    cd rds
    ./build_rds.sh

The RDS is launched using terraform with its own security group and the security group to allow the discovery machine and the selected home IP addresses in the config file full access to it, but is closed to all other IPs.

The schemas infrastructure, gathered, athena, jenkins and procedures will be loaded into it upon launch.
If you wish to add more schemas add the sql file to this folder and edit the null resource *upload_schema* in the file *rds/rds.tf* to include this upload.

### Setup of Google Service Account and Sheet

The script should run in an activated Python3 virtual environment, with the necessary modules installed from requirements.txt.

The steps to set up the Google Account and the required APIs are as follows:

* Set up a new Project and Google Service Account on the Google Console
* Create and download the Google Service Account Credentials
  - Create Key in JSON type
  - Store as ``./GoogleSheets/credentials.json``
* Enable Google Drive API and Google Sheets API for the Project
* Set up the Google Worksheet
  - Create an empty Worksheet (Note: the creation of the WorkSheet can be automated using create() command from gspread - this would also require the use of share() to allow access to certain accounts)
  - Share with the client_email from credentials (finalproject@eternal-wavelet-298111.iam.gserviceaccount.com)

The file ``config.sh`` needs to be set up with the correct variables. The parameters used are:

* __DB_USER__
* __DB_PASS__
* __DB_NAME__
* __DB_HOST__
* __credentials_path__
* __sheet_name__
* __sheet_tab__
* __tables__

The user can change __sheet_name__ to the name of the one created in setup, __sheet_tab__ should be left as an empty string unless the user is only updating one table at a time, and would like the sheet to have a specific name - if left empty, the sheet will be given the name of the database table. __tables__ is a list of the tables from the database that the user would like to pull to the Spreadsheet.

## Gathering data
The scripts to gather data should be run on the discovery machine.

### Logging on to the discovery machine
The public IP address of the discovery machine can be found by running

    cd discovery_machine/terraform_build
    terraform output discovery_public_ip

This IP can be used to log onto the machine with the project key which was made when the discovery machine was created, using the command

    ssh -i <pathToProjectKey> ec2-user@<discoveryIP>

#### Accessing the project key
For easy access to the discovery machine during the project, you can save this key to your own .ssh directory.

First set the environment by running

    ./setenv.sh

then download the key by running

    python3 keys/get_key.py

Once you have completed the project, you can remove the key again by running

    python3 keys/remove_key.py

### Setting the environment
Before running any discovery scripts you must set the environment on the discovery machine. There are two stages to this, the configuration environment and the python virtual environment.

First, git clone this repository into the ec2-user home directory by running

    git clone git@bitbucket.org:KimSpijkersShaw/final_project_academy.git

Next copy up your config file, containing all of the correct variables by running

    scp -i <pathToProjectKey> config ec2-user@<discoveryIP>:final_project_academy/config

on your local machine.

Then, in the root directory of this project on the discovery machine, run

    source ./setenv.sh

Now the key variables will be set to be grabbed by the data gathering scripts.

To make your python virtual environment run

    python3 -m venv ~/<venv_name>

where <venv_name> is the name you would like to give your virtual environment.

Then to activate it and intall the correct modules run

    source ~/<venv_name>/bin/activate
    cd final_project_academy
    pip install -r requirements.txt

This might take some time as all of the dependencies are installed.
The first line should be run to activate the virtual environment every time you log in and wish to run discovery scripts.

## Infrastructure Data Collection

The python scripts located in the infra_discovery directory collect data on AWS infrastructure using Boto3 and upload it to a mysql database specified in environment variables or parameters provided when they are executed. Example output from running --help with the *lb_data.py* script is shown below.

    usage: lb_data.py [-h] [-r REGION] [-p PROFILE] [-du DBUSER] [-dp DBPASS] [-dh DBHOST] [-dn DBNAME]

    Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.

    optional arguments:
    -h, --help            show this help message and exit
    -r REGION, --region REGION
                        The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.
    -p PROFILE, --profile PROFILE
                        The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.
    -du DBUSER, --dbuser DBUSER
                        The username for accessing the mysql database. Overrides value set in DB_USER environment variable.
    -dp DBPASS, --dbpass DBPASS
                        The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.
    -dh DBHOST, --dbhost DBHOST
                        The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.
    -dn DBNAME, --dbname DBNAME
                        The name of the database to use. Overrides value set in DB_NAME environment variable.

To run the scripts, a Python virtual environment should be created with the following modules installed:

* sys
* os
* argparse
* json
* boto3
* botocore
* mysql.connector
* pandas

The config file should be setup with the correct variables. The parameters used by the infrastructure discovery scripts are as follows:

* region
* profile
* client-vpc-id
* project security group id (which is read from project s3 during set environment)
* DB_USER
* DB_PASS
* DB_NAME
* DB_HOST

**LOADBALANCERS, RDS, ROUTE 53 AND S3 BUCKETS:**

* lb_data.py
* rds_data.py
* route53_data.py
* s3_data.py

Collect information on the loadbalancers, rds', route 53 records that correspond with the loadbalancers, and s3 buckets within the clients infrastructure.

N.B. The route 53 script CANNOT be run before the loadbalancer script as it searches for records that correspond with the dns names in the load balancer table in the database.

**INSTANCES, SUBNETS, SECURITY GROUPS, IAM INSTANCE PROFILE ASSOCIATIONS**

Collects information on the instances, subnets, security groups and iam instance profiles associations. The scripts for each resources are split into a modular manner so they can be carried out independantly or together. Each resource has two scripts associated with their discovery.

The first script is to describe and collect the relevant information of the resource, following which creates a local json file containing that specific data. The second script sorts and imports the data to the database.

The scripts to gather the data on these resources is wrapped in a master script which will gather the data on instances, subnets, security groups and iam instance profile associations. all this data is the processed and uploaded to the database.

The individual modules can be ran to either collect the data and store in local json files or to import the json files to the database. These script can be carried out individually by passing the script name in the command line, as shown below, or by calling the functions within the scripts if within a python shell.

`` ./infra_discovery/<script_name>.py ``

This can be carried out by running the following command from the root project directory:

`` ./infra_discovery/ec2_main.py ``

The *ec2_main.py* script connects to the database and populates the following tables:

* ec2
* ec2_sg
* subnets
* routes
* sg_ingress_rules
* sg_egress_rules
* security_groups
* iam_inst_profile

## Traffic and Connectivity Data Collection

### Traffic

Using scripts found in the `` ./trafficTests ``  directory, traffic data can be collected, processed and stored in a database.
#### Traffic Data Collection

In order to collect data on the traffic within the infrastructure being explored, the following scripts must be executed on the discovery machine from within the ``final_project_academy`` directory.

###### Order of operation

- ``./trafficTests/Nmap.py``
- ``./trafficTests/TCPdump.py``
- ``./trafficTests/copyTCPdump.py``
- ``./trafficTests/readTCPdump.py``

##### Nmap

 The purpose of this script is to find all the open ports on each ec2 instance and save these to the discovery database. The following procedures are carried out:

 *  Queries the discovery database for the private IP addresses of all previously discovered ec2 machines.
 * Loops through each of these private IP addresses and runs the command ``nmap`` on the discovery machine against these addresses.
 * This command scans every port on the machine with that IP and collects the **port number**, **port protocol**, and **port service** for every open port it finds.
 * Machine IP address, open port numbers, port protocols, and port services are then sent to the table **open_ports** in the discovery database.

###### TCPdump
 The purpose of this script is to start the command-line utility TCPdump on all available ec2 machines in the infrastructure. The TCPdump command will collect traffic data in ``.pcap`` form. The following procedures are carried out:

 * Queries the discovery database for the private IP addresses of all previously discovered ec2 machines.
 * Loops through each private IP and queries the open_ports table in the discovery database for open ports associated with this IP, as well as querying the rds database for any ports that databases are listening on. These ports are saved to a .txt file per IP.
 * Queries the machine flavour (Debian/RHEL) corresponding to each ec2 machine from the ec2 table.
 * Uses return of machine flavour query to copy up ``runTCPdump.sh`` and relevant``ports.txt`` to each machine, before SSHing onto the machine and running ``./runTCPdump.sh``.

###### runTCPdump shell script
This script does not need to be run by the user, as it is called by the ``TCPdump.py`` script. The following procedures are carried out on each available machine by the script:

* Installs tcpdump if it is not installed.
* Creates ``/var/log/discovery`` directory on the machine to store .pcap file in.
* Loops through list of ports in ``ports-'privateIP'.txt`` and runs tcpcommand against the IP and each port. This is carried out in the background using ``nohup``, and so exiting from the machine will not stop the data collection.
* Exits from the machine.

#### copyTCPdump ####
The purpose of this script is to copy the ``.pcap`` files from the ec2 machines within the client's infrastructure to the discovery machine. The following procedures are carried out by the script:

* Calls procedure to get all private IPs from the ec2 table in the discovery database.
* Calls procedures to get all open ports on each machine and rds ports from the database.
* Loops through IPs and:
    * Kills TCPdump processes currently running on the machine.
    * Loops through ports and copies down the .pcap file with each corresponding port number in it's name.
    * Checks the local ``results`` directory on the discovery machine for the copied down .pcap file.

#### readTCPdump
This script is used to read the data from the downloaded .pcap files, and send relevant extracted data to the discovery database. The following tasks are carried out:

* Convert all .pcap files in the ``./results`` directory to .json files.
* Loops through all .json files and, using a previously defined function, extracts the **source IP**, **source port**, **destination IP**, **destination port**, **packet ID**, **packet length**, **timestamp**, and **frag offset** for each frame of data gathered, and send all of these pieces of data to separate lists.
* Removes unwanted ethernet addresses and duplicated packet lengths from extracted data.
* Determines the length of the lists, and loops through this number, sending the data row by row to the **all_traffic** discovery database.
* Executes the ``readTCPdump.sh`` script with discovery database **user**, **password**, **host**, and **name** as arguments. .

###### readTCPdump shell script
This script does not need to be run by the user, as it is called by the ``readTCPdump.py`` script. This script is used to remove data in the discovery database about traffic to and from the discovery machine, as this is irrelevant to the client.

* Gets the private IP of the discovery machine currently in use.
* Runs MySQL command against the discovery database. This command calls a procedure that removes all rows from the **all_traffic** table where the IP is the IP of the discovery machine.

#### Traffic Data Processing
In order to process the traffic data collected, the following scripts must be executed on the discovery machine from within the ``final_project_academy/trafficTests/dataprocessing`` directory.

###### Order of operation
- ``./makeTrafficrouting.sql``
- ``./makeTrafficrequests.py``
- ``./makeTrafficdatasums.py``

#### makeTrafficrouting
This is a MySQL script and so must either be run with the correct mysql credentials on the command line , or run through use of the MySQL workbench. This script has two uses:

* Creates a routing table of all traffic discovered called **traffic_routing**, with repeated rows removed so only individual paths between machines and ports can be seen.
* Creates a filtered routing table from **traffic_routing** called **traffic_routing_filtered**. This table shows only routes between machines within the VPC, including ec2 instances and RDS machines. Traffic routes to or from external IPs are not shown in this table.

#### makeTrafficrequests
This is a python script and so can be ran on the discovery machine directly using ``python3 makeTrafficrequests.py``. The purpose of this script is to calculate the number of requests made to and from each IP and port. The private IPs of all machines gathered so far are looped through, and the number of rows with each of these IPs and corresponding ports are counted and stored in a table called **traffic_requests** through use of stored MySQL procedures.

#### makeTrafficdatasums
This is a python script and so can be ran on the discovery machine directly using ``python3 makeTrafficdatasums.py``. The purpose of this script is to calculate the amount of data received by any instance and port, and where this data was sent from. All private IPs are looped through, and stored procedures are called that count the number of rows where the current IP is the destination IP and where the current IP is the source IP. This number is then multiplied by the amount of bytes each packet held to give a column of **total_data** received or transmitted by that address and port. All data calculated in this script is stored in the **inbound_traffic** table.

#### Additional processes for future sprints
- ``./makeTrafficovertime.py``

If data was gathered over a larger time period, we would analyse the times of day at which traffic was lowest/highest.

##### Re-running traffic data collection and processing
If you want to re-run processing, drop all traffic tables and re-run gathered.sql before re-running the Nmnap and TCPdump scripts.


## OS Data Collection

The information about the OS versions which are running on the machines can be collected and send to the database by running the scripts from os_discovery/ directory. The main script get_os.sh calls Python script to get the private IPs of the instances, logs on to them and extracts OS information from the appropriate files. Next, the data gathered is send to the database using the script send_os_info.py

In order to perform OS data collection simply run:

    cd os_discovery/
    ./get_os.sh

In order to run, the scripts require access to a set of Python libraries, therefore using a virtual environment consisting of the following modules is highly recommended:

* sys
* mysql.connector
* argparse

The scripts are also accessing variables which can be set in the configuration file. It is important to make sure that the following variables are correctly set up:

* DB_USER
* DB_PASS
* DB_NAME
* DB_HOST

**At the moment only Linux OS is supported. Therefore, the script's main purpose is to identify the flavour of the Linux system.** This information is then used at later stages of the discovery process.

**Currently supported Linux OS flavours:**

* Debian
* RHEL

## CloudWatch Data Collection

The monitoring data for EC2 Instances, Load Balancers and RDS Instances can be collected and uploaded to the database using the Python scripts located in Cloudwatch/ directory. Each one of them is a self-contained script which can be run on its own by providing a set of arguments.

Each script works by:
1. Accessing the database for the IDs of instances present in the system.
2. Constructing an API request for a set of data statistics which have been collected over the past 7 days.
3. Contacting the AWS and coming back with the desired results.
4. Sending gathered information to the database and organising it, so that each metric is stored in exactly one column inside a table. One table is utilized per one set of requests.

All scripts can be launched by running:

    cd Cloudwatch/
    ./<script_name>.sh [-r REGION] [-du DBUSER] [-dp DBPASS] [-dh DBHOST] [-dn DBNAME]

    optional arguments:

    -r REGION, --region REGION
                        The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.
    -du DBUSER, --dbuser DBUSER
                        The username for accessing the mysql database. Overrides value set in DB_USER environment variable.
    -dp DBPASS, --dbpass DBPASS
                        The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.
    -dh DBHOST, --dbhost DBHOST
                        The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.
    -dn DBNAME, --dbname DBNAME
                        The name of the database to use. Overrides value set in DB_NAME environment variable.

In order to run, the scripts require access to a number of Python libraries, therefore using a virtual environment consisting of the following modules is highly recommended:

* sys
* os
* argparse
* boto3
* botocore
* mysql.connector

The scripts are also accessing variables which can be set in the configuration file. It is important to make sure that the following variables are correctly set up:

* region
* DB_USER
* DB_PASS
* DB_NAME
* DB_HOST

**EC2 METRICS:**

* CPUUtilization [%]
* Number of Disk Read Operations [Count]
* Number of Disk Write Operations [Count]
* Disk Read Operations [Bytes]
* Disk Write Operations [Bytes]

The statistic used for each metric is **Maximum** for the sampling period of 5 minutes.

**LOADBALANCER METRICS:**

* Latency [Seconds]
* Number of Requests [Count]

The statistic used for Latency metric is **Average** and for Number of Request **Sum**, both with the sampling period of 5 minutes.

**RDS METRICS:**

* ReadLatency [Seconds]
* WriteLatency [Seconds]
* ReadThroughput [Bytes per Seconds]
* WriteThroughput [Bytes per Seconds]

For each metric, the returned values are **Average** over the sampling period of 5 minutes.

## Discovery Agent

This discovery tool utilizes the AWS Discovery Agent. AWS Discovery Agent is a piece of software installed on on-premise servers targeted for discovery and potential migration. Agents capture system configuration, system performance, running processes, and details of the network connections between systems. They run in local environment and require root privileges.

When the Discovery Agent is started, it connects securely with the home region and registers with the Migration Hub. It pings the service at 15-minute intervals for configuration information. When a command that tells an agent to start data collection is sent, agent starts collecting data for the host where it resides. This happens automatically since the command is included in the installation script. The collected data is stored in a designated S3 bucket in parquet format. All of these data points can be retrieved and subsequently used to map IT assets and their network dependencies, help to determine the cost of running these servers in AWS and also plan for a potential migration.

### Installing Agent

**Requirements**

In order to successfully install and run the Discovery Agent, a set of requirements has to be met:

1\. All the instances on which the agent will be installed need to have port 443 open to all outbound traffic. This is guaranteed by the security group attached to all the EC2 instances by the script building the discovery infrastructure.

2\. Migration Hub Home Region has to be set on your AWS account. Then, you should assign this region to the mgh_home parameter inside the configuration file. If you have not yet set up your home region, the script will attempt to try and set it up for you using the region specified by the variable mgh_home_region. *However, it is advisable to do this process manually since there still happen to some issues with the automated script.*

3\. In order to run, the discovery agent scripts require access to a set of Python libraries, therefore using a virtual environment consisting of the following modules is highly recommended:

* sys
* mysql.connector
* argparse

4\. The scripts are also accessing variables which can be set in the configuration file. It is important to make sure that the following variables are correctly set up:

* DB_USER
* DB_PASS
* DB_NAME
* DB_HOST
* REGION
* AWS_ACCOUNT_ID
* MGH_HOME

5\. Installing the Discovery Agent software on the instances also requires a set of IAM user credentials, including *secret access key* and *access key ID*. This requirement is met by the automated script which creates a user and assigns to it the necessary policies which include:

* Current policies attached to the instance_profile assigned to the instance.
* Set of policies required strictly by the Discovery Agent. That is: IAMFullAccess, AWSApplicationDiscoveryAgentAccess, AmazonS3FullAccess, AWSDiscoveryContinuousExportFirehosePolicy, AWSApplicationDiscoveryServiceFullAccess

**Running the script**

      cd discovery_agent/
      ./add_agent.sh

The script will automatically pull the variables from the config file and contact discovery database to access any additional information. No other action should be required to complete the agent installation. Note that it might take a couple of minutes for the Discovery Agents to start sending data to the newly created S3 bucket.

**Uninstalling the Agent**

After all the desired data has been collected by the Agents, it can be easily deactivated and uninstalled with one command.

      cd discovery_agent/
      ./remove_agent.sh

Note that this command removes ALL Agents from ALL machines as well as removes any IAM users crated in the process. The command for removing one Agent from a specific instance is not yet available and the process has to be performed manually.

### Data Collection from AWS Discovery Agent through Athena

This section uses Amazon's Athena service to query the data from the Agents, then send the results to the database.

The file ``config.sh`` needs to be set up with the correct variables. The parameters used by the Athena scripts are:

* __chosen_region__
* __profile__
* __bucket_name__
* __DB_USER__
* __DB_PASS__
* __DB_NAME__
* __DB_HOST__

The script ``./Athena/athena_outputs`` will execute queries within Athena and send the outputs to the S3 bucket.

It also calls ``./Athena/csv_to_database``, which saves the bucket csv files locally, runs ``./Athena/read_csv.py``, then deletes the local csv files

``./Athena/read_csv.py`` connects to the database and populates 7 tables with output data:

* idmapping
* inboundconnection
* networkinterface
* osinfo
* outboundconnection
* processes
* sysperformance

## Machine Install Data Collection
This section describes the script that gather information about the packages and programs installed on each of the EC2 instances in the client's system.

This can be useful for finding application pipelines and for finding install requirements for direct rehosting of applications.

These scripts are all found in the find_installs directory.

They should all be run on the discovery machine as they use private IP addresses to access the client machines.

Currently they only have the capability to gather information about linux machines, but they have been written in a modular form to allow scripts to run on other operating systems to be added in later sprints. If a machine has another operating system a notification will be sent to the terminal but no error will be raised in order to allow the script to run for the maximum number of machines possible.

### Package Install Lists
To collect a list of all of the packages installed on a machine, run

    cd find_installs/packages
    python3 find_installed.py

This script will pull the private IP address and operating system information from the ec2 table in the database and use that to log into each machine and get a list of installed packages.

It will save this into a file called packages-<public_ip>.txt for each machine, which will be stored in the project S3 bucket under the key package-installs.

### Finding Running Programs
To gather information about the processes currently running on a machine and the programs that are running them, run

    cd find_installs/processes
    python3 find_processes.py

This script will:

- Pull the private IP address and operating system information from the ec2 table in the database.
- Log into each machine and use netstat to find information about running processes on the ports of the machine.
    - If Netstat is not already installed, it will be installed, used and uninstalled, leaving the client machine in its original state.
- Use the process IDs found to get information about the command that generated a given process.
- Send all the gathered information to the database table port_process_info

The information it gives is the Process ID (PID), process name, protocl, local address it is running on, foregin address it is connecting to, state (Listening for example) and command used to run the process.

Once this data has been collected, it can be used to find further information about docker containers that are running on the machines and jenkins pipelines.

This data will be linkes to the instance ID of the instance in the database.

### Docker Containers
To get information about what docker containers any machines using docker are running, run

    cd find_installs/docker
    python3 get_docker_info.py

This will:

- Pull the IP addresses and operating system information about any machines running docker (gathered in the find running programs section above) from the database.
- Access the machines and use docker to find information about the containers running.
- Send the gathered information to the databse table docker_containers.

If none of the client machines are running docker, this script will do nothing.

The information it collects about each container is container ID, container image, command it ran, when it was created, status (running or not), ports it has open and where they map to on the host machine, networks it is connected, to, container size, the number of local volumes attached, its names and its labels.

This information will be linked to the instance ID of the instance it is running on in case there are multiple instances running docker.

### Jenkins Jobs
To get information about the jekins jobs on a machine running jenkins, in order to determine the pipeline, run

    cd find_installs/jenkins
    python3 get_jenkins_info.py

This will:

- Pull the IP addresses and operating system information about any machines running jenkins (gathered in the find running programs section above) from the database.
- Copy down job configuration information.
- Extract useful parts of this and send them to the database.
- Delete the copy of the job configuration information.

The extracted job configutarion information will be split into four tables:

- jenkins_jobs gives an overview of each job: the job name, job description and job state (enabled or disabled).
- jenkins_job_parameters gives information about the parameters for parameterised jobs: the parameter name, parameter description and default value.
- jenkins_jobs_scm gives information about source code referenced in the job: the class of source code, the jenkins plugin used to gather it and for git repositories, the repository url and the branch being used.
- jenkins_jobs_triggers gives information about the automatic triggers of the job: the trigger type (curretly reverse build trigger or scheduled job), the updtream job and trigger threshold for reverse builds and the cron format scheduling for scheduled builds.
- jenkins_job_builders gives information about the build steps the job includes: the builder type (currently only shell) and the shell command it runs for shell builds.

The information in each table will be linked to the instance ID and the job name so that it is clear where each job is running.

Some of these currently have limited functionality which would need to be extended if other job types were used:

- jenkins_job_scm can only gather information about git repositories.
- jenkins_job_triggers can only gwther information about scheduled jobs and jobs triggered by builds of other jobs.
- jenkins_job_builders can only gather information about jobs which execute shell scripts.

Other jobs types shouldn't raise errors, there will just be some missing information.

## Retrieving Data

Using and analysing the data we have found during Discovery.

### Google Sheets API
The 'Setup' section details the setting up of a Google Service Account, a Google Sheets Worksheet and enabling the Google APIs. Once these are completed and the database is populated with tables of discovery data, the Sheet can display the data by running the command ``run_all.sh``

The script will prompt the user with a menu of tables. These are tables in the database that may be sent to the Sheet, updating it with the database's current values;

* If the user chooses option 1) then all of the tables will be updated in turn.
* If the user chooses any option between 2) and 42) then the table of choice only will be updated
* If the user chooses the final option then the script will end there.

The script ``run_all.sh`` exports the relevant __dbtable__ variable then calls on ``database_run.py``. This then finds the necessary environment variables set by ``setenv.sh`` and calls the updatesheet function defined in ``database_function.py``:
* updatesheet takes 5 arguments: __sheet_name__, __sheet_tab__, __dbtable__, __credentials_path__ and __dbname__, then connects to the database, runs MySQL commands to retrieve database information and uses the Google Sheets API to create tables in a Google Drive Worksheet.

__Note:__ A possible error that may be raised is an 'APIError', from ``gspread.exceptions.APIError``; this will come up when the quota limit for write requests per minute per user is exceeded. To try to avoid this happening, ``run_all.sh`` puts a delay of 20 seconds between each table iteration (when option 'ALL' is selected), and for larger tables, a small delay between each row.

### Automated Graphing

The folder pythonGraphs in the root directory aims to automate graphs taken from data stored in the Google Sheet.

#### database_get.py

``database_get.py`` acts as a wrapper script to call the functions that will create relevant graphs for each table. The tables that are currently included to be visually represented are sysperformance and traffic_requests.

Environment variables __sheet_name__ and __cr_path__ are set using argparse.

#### getpandas function

getpandas is defined in ``func_getpandas.py``, to be called with the arguments __sheet_name__, __cr_path__ and __sheet_tab__ for each table required. The function connects to the google sheet and pulls out the table to a pandas DataFrame. The dataframe is returned to database_get.py and subsequently passed as an argument for the next function.

#### sysplot function
After the pandas DataFrame is created for the table, the sysplot function defined in ``func_sysperf.py`` is run. The function is specific for the sysperformance data, and creates 2 plots in total:

* 'CPU Usage from Agent over Discovery Time Period'
  - This line graph plots the percentage of the CPU being utilized against time, over the 5-day period that was measured.
* 'Disk Bytes written over Discovery Time Period'
  -  This line graph plots both the number of kilobytes that was written to the disk and the amount of disk write operations per second, against time.

#### trafficreqplot function

getpandas is called again with a different __sheet_tab__ argument for the traffic_requests table, so that the function trafficreqplot, defined in ``func_trafficreq.py``, can create plots specific to the Traffic Requests data.

1 plot is created, a column bar chart describing the 'Total Traffic Requests on each machine' - the graph plots each Machine IP against the total number of requests identified to that machine.

## Post discovery
There are automatic scripts to destroy the extra infrastructure and security rules that were created to run the discovery.

They must be run in the order listed here as some things are dependent on others.

### Destroying the discovery RDS
Make sure all desired data has been pulled from the RDS into your desired data display system before destroying it.

To destroy the discovery RDS (along with its subnet groups and security groups) run

    cd rds
    ./destroy_rds.sh

### Destroying the discovery machine
Run the script

    cd dicovery_machine
    ./delete_discovery

This will:

- Remove the discovery_allow security group from all of the client's machines it was applied to, leaving them as they were before discovery began.
- Delete the discovery machine.
- Delete the security groups and iamrole created for the discovery machine.

### Destroying the s3 bucket
Do not destroy the s3 bucket until both the rds and the discovery machine have been destroyed as their terraform statefiles are stored in it.

To destroy the bucket run

    cd discovery_storage
    ./delete_s3.sh
