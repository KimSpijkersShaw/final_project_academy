#!/bin/bash

# Store all input variables:
# AWS region
# AWS account id
# private IP address of the instance
# AWS Migration Hub home region
# Linux flavour of the instance OS
# AWS secret access key of the IAM user
# AWS access key ID of the IAM user

reg=$1
ACCID=$2
address=$3
home_region=$4
flavour=$5
acc_key=$6
key_id=$7

#Check if the MGH home region is set
if ! aws migrationhub-config get-home-region --region $reg 1>/dev/null 2>&1
then
  echo "Migrationhub home region is NOT set!"
  echo "Setting the home region..."

  aws migrationhub-config create-home-region-control --home-region $reg \
  --target Type=ACCOUNT,Id=$ACCID --region $reg

  if ! aws migrationhub-config get-home-region --region $reg 1>/dev/null 2>&1
  then
    echo "Failed to set migrationhub home region!"
  fi
fi

#Check if the agent is already installed
if sudo systemctl status aws-discovery-daemon.service
then
  echo "The discovery agent is already installed"
  echo "Exiting..."
  exit 2
fi

#Create temporary workspace directory for the discovery agent
cd ~
mkdir agent
cd agent

#Download the agent
curl -o ./aws-discovery-agent.tar.gz \
https://s3-us-west-2.amazonaws.com/aws-discovery-agent.us-west-2/linux/latest/aws-discovery-agent.tar.gz

#Verify the cryptographic signature of the installation package
curl -o ./agent.sig https://s3.us-west-2.amazonaws.com/aws-discovery-agent.us-west-2/linux/latest/aws-discovery-agent.tar.gz.sig
curl -o ./discovery.gpg https://s3.us-west-2.amazonaws.com/aws-discovery-agent.us-west-2/linux/latest/discovery.gpg
if ! gpg --no-default-keyring --keyring ./discovery.gpg --verify agent.sig aws-discovery-agent.tar.gz
then
  echo "Cryptographic signature verification FAILED!"
  echo "Exiting..."
  exit 3
fi

#Extract from the Tarball
tar -xzf aws-discovery-agent.tar.gz

#Install the agent
sudo bash install -r $home_region -s $acc_key -k $key_id

#Activate the agent
sudo systemctl restart aws-discovery-daemon.service
#Wait for it to restart
sleep 15

#Install jq
if [[ $flavour == "rhel" ]]
then
  sudo yum install -y jq
elif [[ $flavour == "debian" ]]
then
  sudo apt update
  sudo apt install -y jq
fi

# Extract and store agent ID
agent_id=$(aws discovery describe-agents --region $reg | jq -r '.agentsInfo[]' | sed -n "/$address/,\$p" | grep -m 1 "agentId" | awk '{print $2}' | cut -d'"' -f2)

#Check if the agent is up and running
if [[ ! -z $agent_id ]]
then
  echo "SUCCESS! Discovery Agent is up and running!"
  # Start data collection
  aws discovery start-data-collection-by-agent-ids --agent-ids $agent_id
  sudo systemctl enable aws-discovery-daemon.service
else
  # If installation of agent failed revert changes
  echo "Setting up of discovery agent FAILED!"
  echo "Reverting the changes..."
  sudo systemctl stop aws-discovery-daemon.service
  if [[ $flavour == "rhel" ]]
  then
    sudo yum remove -y aws-discovery-agent
  elif [[ $flavour == "debian" ]]
  then
    sudo apt remove -y aws-discovery-agent
  else
    echo "Agent could NOT be uninstalled automatically!"
  fi
  rm -fr ~/agent
fi
