#!/bin/bash

# Source vars from config file
source ../setenv.sh

delete_user () {
  """Function which deletes IAM user. Takes 1 argument -> username"""
  user=$1

  for policy in $(aws iam list-attached-user-policies --user-name $user | jq -r ".AttachedPolicies[].PolicyArn")
  do
    aws iam detach-user-policy --user-name $user  --policy-arn $policy
  done

  key_id=$(aws iam list-access-keys --user-name $user | jq -r ".AccessKeyMetadata[].AccessKeyId")
  aws iam delete-access-key --access-key-id $key_id --user-name $user

  if aws iam get-user --user-name $user >/dev/null 2>&1
  then
    aws iam delete-user --user-name $user >/dev/null 2>&1
    if aws iam get-user --user-name $user >/dev/null 2>&1
    then
      echo "User $user could not be deleted"
    else
      echo "User $user successfully deleted!"
    fi
  else
      echo "User $user has been already deleted!"
  fi
}

# Get instance_id's of instances where the agent should be removed
./get_instanceIDs.py > tempfile.txt
readarray instances < tempfile.txt
rm tempfile.txt
for ec2 in "${instances[@]}"
do
  # Get the current attached instance profile
  profile=$(./get_profiles.py $ec2)
  if [[ -z $profile ]]
  then
    ################ If no instance profile attached to the instance
    # Get the private IP of the instance
    address=$(./get_IPs.py $ec2)
      # Check the flavour of Linux
      if [[ $(./get_os.py $address) == "rhel" ]]
      then
        # Remove IAM user credentials and discovery agent from the remote machine
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ec2-user@$address "sudo systemctl stop aws-discovery-daemon.service; sudo yum remove -y aws-discovery-agent; rm -fr ~/.aws/"
      elif [[ $(./get_os.py $address) == "debian" ]]
      then
        # Remove IAM user credentials and discovery agent from the remote machine
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ubuntu@$address "sudo systemctl stop aws-discovery-daemon.service; sudo apt remove -y aws-discovery-agent; rm -fr ~/.aws/"
      else
        # Show error message if the OS flavour cannot be identified
        echo "Failed to connect to the remote machine [OS/Linux flavour not supported]"
      fi
  else
    ################ If instance profile is attached to the instance
    # Get the private IP of the instance
    address=$(./get_IPs.py $ec2)
    # Check the flavour of Linux
      if [[ $(./get_os.py $address) == "rhel" ]]
      then
        # Remove IAM user credentials and discovery agent from the remote machine
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ec2-user@$address "sudo systemctl stop aws-discovery-daemon.service; sudo yum remove -y aws-discovery-agent; rm -fr ~/.aws/"
      elif [[ $(./get_os.py $address) == "debian" ]]
      then
        # Remove IAM user credentials and discovery agent from the remote machine
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ubuntu@$address "sudo systemctl stop aws-discovery-daemon.service; sudo apt remove -y aws-discovery-agent; rm -fr ~/.aws/"
      else
        # Show error message if the OS flavour cannot be identified
        echo "Failed to connect to the remote machine [OS/Linux flavour not supported]"
      fi
    # Remove the user from AWS using function delete_user
    user_suffix=$(echo "$ec2" | awk -F'-' '{print $2}')
    delete_user "Agent$user_suffix"
  fi
done

# Delete user Agent
delete_user "Agent"
