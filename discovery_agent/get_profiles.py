#!/usr/bin/env python3

import sys
import mysql.connector
import argparse

# Grab instance ID from the input args
ec2_id = str(sys.argv[1])

# Get variables from config file
parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

# Connect to the database
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)

# Add cursor
cur = dbconn.cursor(dictionary=True, buffered=True)

# Get instance profile arn and print it out
selectsql = "SELECT arn FROM iam_inst_profile WHERE instance_id=\'"+ ec2_id +"\';"
cur.execute(selectsql)
info = cur.fetchall()

for instance in info:
  print(instance['arn'])

# Close DB connection
cur.close()
dbconn.close()
