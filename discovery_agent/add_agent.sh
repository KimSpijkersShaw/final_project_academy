#!/bin/bash

# Source vars from config file
source ../setenv.sh

reg=$REGION
ACCID=$AWS_ACCOUNT_ID
home_region=$MGH_HOME

# Vars for storing IAM user credentials
default_acc_key=""
default_key_id=""

# Create IAM user Agent and store its credentials
if aws iam get-user --user-name Agent >/dev/null 2>&1
then
  echo "User Agent already exists"
else
  aws iam create-user --user-name Agent >/dev/null 2>&1
  user_info=$(aws iam create-access-key --user-name Agent)
  default_acc_key=$(echo "$user_info" | jq -r '.AccessKey.SecretAccessKey')
  default_key_id=$(echo "$user_info" | jq -r '.AccessKey.AccessKeyId')

  # Check if user Agent has been successfully created
  if aws iam get-user --user-name Agent >/dev/null 2>&1
  then
    echo "User Agent successfully created!"
  else
    echo "User Agent could not be created"
    echo "Exiting..."
    exit 1
  fi
  # Attach necessary policies for discovery agent to work
  aws iam attach-user-policy --user-name Agent --policy-arn arn:aws:iam::aws:policy/IAMFullAccess
  aws iam attach-user-policy --user-name Agent --policy-arn arn:aws:iam::aws:policy/AWSApplicationDiscoveryAgentAccess
  aws iam attach-user-policy --user-name Agent --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
  aws iam attach-user-policy --user-name Agent --policy-arn arn:aws:iam::aws:policy/AWSDiscoveryContinuousExportFirehosePolicy
  aws iam attach-user-policy --user-name Agent --policy-arn arn:aws:iam::aws:policy/AWSApplicationDiscoveryServiceFullAccess
fi

# Get instance_id's of instances where the agent should be installed
./get_instanceIDs.py > tempfile.txt
readarray instances < tempfile.txt
rm tempfile.txt
for ec2 in "${instances[@]}"
do
  # Get the current attached instance profile
  profile=$(./get_profiles.py $ec2)
  if [[ -z $profile ]]
  then
    ################ If no instance profile attached to the instance
    # Get the private IP of the instance
    address=$(./get_IPs.py $ec2)
      # Check the flavour of Linux
      if [[ $(./get_os.py $address) == "rhel" ]]
      then
        flavour="rhel"
        # Copy install script to the remote host, configure iam user and run the script
        scp -i ~/.ssh/$SSH_KEY_NAME install.sh  ec2-user@$address:~/install_agent.sh
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ec2-user@$address "aws configure set aws_access_key_id $default_key_id; aws configure set aws_secret_access_key $default_acc_key; aws configure set default.region $reg; chmod +x ~/install_agent.sh; ./install_agent.sh $reg $ACCID $address $home_region $flavour $default_acc_key $default_key_id"
      elif [[ $(./get_os.py $address) == "debian" ]]
      then
        flavour="debian"
        # Copy install script to the remote host, configure iam user and run the script
        scp -i ~/.ssh/$SSH_KEY_NAME install.sh  ubuntu@$address:~/install_agent.sh
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ubuntu@$address "aws configure set aws_access_key_id $default_key_id; aws configure set aws_secret_access_key $default_acc_key; aws configure set default.region $reg; chmod +x ~/install_agent.sh; ./install_agent.sh $reg $ACCID $address $home_region $flavour $default_acc_key $default_key_id"
      else
        # If flavour not identified -> exit
        echo "Failed to connect to the remote machine [OS/Linux flavour not supported]"
        echo "Exiting..."
        exit 1
      fi
  else
    ################ If instance profile is attached to the instance
    # Create IAM user Agent+suffix and store its credentials
    user_suffix=$(echo "$ec2" | awk -F'-' '{print $2}')
    aws iam create-user --user-name Agent$user_suffix
    user_info=$(aws iam create-access-key --user-name Agent$user_suffix)
    acc_key=$(echo "$user_info" | jq -r '.AccessKey.SecretAccessKey')
    key_id=$(echo "$user_info" | jq -r '.AccessKey.AccessKeyId')
    # Check if user Agent has been successfully created
    if aws iam get-user --user-name Agent
    then
      echo "User Agent$user_suffix successfully created!"
    else
      echo "User Agent$user_suffix could not be created"
      echo "Exiting..."
      exit 1
    fi
    # Extract profile name from ARN
    profile=$(echo "$profile" | awk -F'/' '{print $2}')
    # Get the role name from the profile name
    role_name=$(aws iam get-instance-profile --instance-profile-name $profile | jq -r '.InstanceProfile.Roles[].RoleName')
    # Get the policies attached to the role
    aws iam list-attached-role-policies --role-name $role_name | jq -r '.AttachedPolicies[].PolicyArn' > tempfile.txt
    readarray policies < tempfile.txt
    rm tempfile.txt
    # Add all 'old' policies to the new IAM user
    for policy in "${policies[@]}"
    do
      aws iam attach-user-policy --user-name Agent$user_suffix --policy-arn $policy
    done
    # Add all policies required by the discovery agent to work
    aws iam attach-user-policy --user-name Agent$user_suffix  --policy-arn arn:aws:iam::aws:policy/IAMFullAccess
    aws iam attach-user-policy --user-name Agent$user_suffix  --policy-arn arn:aws:iam::aws:policy/AWSApplicationDiscoveryAgentAccess
    aws iam attach-user-policy --user-name Agent$user_suffix  --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
    aws iam attach-user-policy --user-name Agent$user_suffix  --policy-arn arn:aws:iam::aws:policy/AWSDiscoveryContinuousExportFirehosePolicy
    aws iam attach-user-policy --user-name Agent$user_suffix  --policy-arn arn:aws:iam::aws:policy/AWSApplicationDiscoveryServiceFullAccess
    # Get the private IP of the instance
    address=$(./get_IPs.py $ec2)
      if [[ $(./get_os.py $address) == "rhel" ]]
      then
        flavour="rhel"
        # Copy install script to the remote host, configure iam user and run the script
        scp -i ~/.ssh/$SSH_KEY_NAME install.sh  ec2-user@$address:~/install_agent.sh
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ec2-user@$address "aws configure set aws_access_key_id $key_id; aws configure set aws_secret_access_key $acc_key; aws configure set default.region $reg; chmod +x ~/install_agent.sh; ./install_agent.sh $reg $ACCID $address $home_region $flavour $acc_key $key_id"
      elif [[ $(./get_os.py $address) == "debian" ]]
      then
        flavour="debian"
        # Copy install script to the remote host, configure iam user and run the script
        scp -i ~/.ssh/$SSH_KEY_NAME install.sh  ubuntu@$address:~/install_agent.sh
        ssh -i ~/.ssh/$SSH_KEY_NAME -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
        ubuntu@$address "aws configure set aws_access_key_id $key_id; aws configure set aws_secret_access_key $acc_key; aws configure set default.region $reg; ./install_agent.sh $reg $ACCID $address $home_region $flavour $acc_key $key_id"
      else
        # If flavour not identified -> exit
        echo "Failed to connect to the remote machine [OS/Linux flavour not supported]"
        echo "Exiting..."
        exit 1
      fi
  fi
done
