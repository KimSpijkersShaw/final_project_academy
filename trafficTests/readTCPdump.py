#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed

# sudo pip3 install pycapkit
# sudo pip3 install emoji

import os.path 
import os
import json
import sys
import subprocess
import mysql.connector
import paramiko
import pcapkit
import argparse

##################################################################################################
# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())

##################################################################################################

# Define function to extract relevant json data
def json_extract(obj, key):
  """Recursively fetch values from nested JSON."""
  arr = []
  def extract(obj, arr, key):
    """Recursively search for values of key in JSON tree."""
    if isinstance(obj, dict):
      for k, v in obj.items():
        if isinstance(v, (list, dict)):
          if isinstance(v, list) and k == key:
            for n in range(len(v)):
              arr.append(v[n])
          else:
            extract(v, arr, key)
        elif k == key:
          arr.append(v)
    elif isinstance(obj, list):
      for item in obj:
        extract(item, arr, key)
    return arr
  values = extract(obj, arr, key)
  return values
       
##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

pubcur = dbconn.cursor(dictionary=True)

##################################################################################################

# Convert all .pcap file in results folder to .json
os.chdir('./results')
currentdir=os.getcwd()
for filename in os.listdir(currentdir):
        newfilename = (filename[:-5] + ".json")
        print(newfilename)
        print(filename)
        pcapkit.extract(fin=filename, fout=newfilename, format='json', extension=False)
      
##################################################################################################

# CREATE DATABASE TABLE OF ALL PACKETS - SOURCE IP & PORT, DESTINATION IP & PORT

# Loop through each .json file in the folder
#os.chdir('./trafficTests')
currentdir=os.getcwd()
print(currentdir)
for jsonfile in os.listdir(currentdir):

    if jsonfile.endswith((".json")):
        
        print(jsonfile)
        
        with open(jsonfile) as json_file:
            readable = json.load(json_file)
       
        # Get timestamp for each frame
        timestamps=(json_extract(readable, 'time'))
        print(timestamps)

        # Get source IP 
        all_src_ips=(json_extract(readable, 'src'))

        # Get source port
        src_ports=(json_extract(readable, 'srcport'))
        print(src_ports)
        
        # Get destination IP 
        all_dst_ips=(json_extract(readable, 'dst'))

        # Get destination port
        dst_ports=(json_extract(readable, 'dstport'))
        print(dst_ports)

        # Get id
        packet_id=(json_extract(readable, 'id'))
        print(packet_id)

        # Get packet length 
        length=(json_extract(readable, 'len'))

        # Get frag_offset
        frag_offset=(json_extract(readable, 'frag_offset'))
        print(frag_offset)

##################################################################################################
        
        # Processing of lists before passing to MySQL database

        # IP lists contain unwanted ethernet addresses too
        # Remove from lists:
        src_ips=[ x for x in all_src_ips if ":" not in x ]
        print(src_ips)

        dst_ips=[ x for x in all_dst_ips if ":" not in x ]
        print(dst_ips)

        # Length list contains duplicated values
        # Remove 1st, 3rd, 5th etc.
        packet_length=length[1::2] 
        print(packet_length)

##################################################################################################
    
        # Get number of elements in each list
        # This determine the number of rows that will be inserted into the database
        number_of_elements = len(dst_ips)
        print(number_of_elements)

        # If number of elements is zero, i.e no traffic detected, break out of loop and move to next file
        if number_of_elements < 1:
            continue 
      
##################################################################################################

        #Per frame, send source IP, source port, destination IP, and destination port, to all_trafficdatabase table
        for n in range(0, number_of_elements):
            print(n)
            print(packet_length)
            print(packet_length[0])
            print(src_ips[0])
            insertsql="INSERT INTO all_traffic(source_ip,source_port,dest_ip,dest_port,timestamp,packet_length,packet_id,frag_offset) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
            sqlvalues=(src_ips[n], src_ports[n], dst_ips[n], dst_ports[n], timestamps[n], packet_length[n], packet_id[n], frag_offset[n])
            try:
                pubcur.execute(insertsql, sqlvalues)
            except mysql.connector.errors.ProgrammingError:
                print("Bad SQL syntax")
                sys.exit(1)
            try:
                dbconn.commit()
            except:
                print("Insert of all traffic information failed")
                sys.exit(1)

       
    else:
        continue

print("Data extraction complete. All extracted data sent to discovery database.")
##################################################################################################

# Execute readTCPdump.sh script to remove unwanted rows from the all_traffic database
os.chdir('../')
executecommand=("./readTCPdump.sh %s %s %s %s" % (args['dbuser'],args['dbpass'],args['dbhost'],args['dbname']))
os.system(executecommand)