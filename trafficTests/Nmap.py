#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 and python-nmap installed

# Script to run nmap against all private IPs located within the infrastructure
# Will find all open port and services running on them, and send to table in database
# Database table will contain machine private IP, open ports, and services running on that port


import os.path 
import os
import json
import sys
import subprocess
import mysql.connector
import paramiko
import pcapkit
import argparse
import nmap


##################################################################################################

# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())

##################################################################################################

# Connecting to database server
print("Attempting to connect to database")
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Use procedure to get all private IPs
all_private_IPs=[]
mycursor.callproc('getAllprivateIPs')
for result in mycursor.stored_results():
    
        records = result.fetchall()
        print("Private IPs are:")
        for item in records:

            print(item[0])
            # Store all private IPs in a list
            
            all_private_IPs.append(item[0])
            
print(all_private_IPs)

# Initialize the port scanner
nmScan = nmap.PortScanner()
print("Scanning all machines for open ports")

# Run loop to get all open ports for each IP address
#all_private_IPs = ['172.31.10.107', '172.31.1.154']
for privateIP in all_private_IPs: 
    
  nmScan.scan(privateIP, '1-65535')
  
  # Loop through all private IPs
  for host in nmScan.all_hosts():

    # Loop through all protocols
    for proto in nmScan[host].all_protocols():

        lport = nmScan[host][proto].keys()
        sorted(lport)
      
        # Loop through all open ports
        for port in lport:

            # Need to loop through each open port to send each value to database seperately 
            privateIPaddress=('%s' % (host))
            port_protocol=('%s' % proto)
            open_port=('%s' % (port))
            port_service=('%s' % (nmScan[host][proto][port]['name']))
            
            # Print IP, protocol, port, and service on port
            print(privateIPaddress)
            print(port_protocol)
            print(open_port)
            print(port_service)

            print("---------------")

            # Send open port information to database
            insertsql="INSERT INTO open_ports(private_ip,protocol,port_number,service) VALUES(%s,%s,%s,%s)"
            sqlvalues=(privateIPaddress, port_protocol, open_port, port_service)
            try:
                mycursor.execute(insertsql, sqlvalues)
            except mysql.connector.errors.ProgrammingError:
                print("Bad SQL syntax")
                sys.exit(1)
            try:
                dbconn.commit()
            except:
                print("Insert of port information failed")
                sys.exit(1)
            
            
print ("Scanning complete!")
