#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed

import os.path 
import json
import sys
import mysql.connector
import os
import subprocess
import paramiko
import argparse

##################################################################################################

# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())

##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Copy down all .pcap files to discovery machine
# Loop through private IPs
# Use procedure to get all private IPs from database and write to list
all_private_IPs=[]
mycursor.callproc('getAllprivateIPs')
for result in mycursor.stored_results():
        records = result.fetchall()
        print("Private IPs are:")
        for item in records:
            all_private_IPs.append(item[0])

print(all_private_IPs)

# Loop through all public IPs of available machines
for privateIP in all_private_IPs: 

        print(privateIP)

        # Call procedure to get all open ports for given private IP
        portsbyip = []
        mycursor.callproc('PortsByPrivateIP', [privateIP])
        for result in mycursor.stored_results():
                records = result.fetchall()
                for item in records:
                        #print(item[0])
                        # Store all open ports for this machine in a list
                        portsbyip.append(item[0])
                print(portsbyip)

        # Call procedure to get all RDS ports
        rdsports = []
        mycursor.callproc('getDBports')
        for result in mycursor.stored_results():
                records = result.fetchall()
                for item in records:
                        #print(item[0])
                        # Store all open ports for this machine in a list
                        rdsports.append(item[0])
                print(rdsports)
    
        # Add ports by ip and rds ports list together
        ports = rdsports + portsbyip
        print(ports)

        # Call procedure to get machine type for each IP (RHEL/Ubuntu/etc.)
        mycursor.callproc('FlavourByPrivateIP', [privateIP])
        for result in mycursor.stored_results():
                records = result.fetchall()
                for item in records:
                        print(item[0])
                        machineType=item[0]
        

        # Set username for SSH dependent upon machine type
        if machineType == 'rhel' or machineType == 'amazon':

                sshUser="ec2-user"

        elif machineType == 'debian':

                sshUser="ubuntu"

        else:
                print("Machine type found in database is invalid")
                sys.exit(2)
        
        # Kill TCPdump process before capturing to avoid errors
        # SSH onto each machine 
        ssh_key=args['ssh_key_name']
        killcmd="ssh -i ~/.ssh/" + ssh_key + ".pem " + sshUser + "@" + privateIP + " sudo killall tcpdump "  
        os.system(killcmd)
                
        for thisPort in ports: 

                # Copy down .pcap files from each machine in the infrastructure to the discovery machine
                pcapfilename=("/var/log/discovery/IP%sp%s.pcap" % (privateIP, thisPort))
                #newfile=("IP%sp%s.pcap" % (privateIP, thisPort))
        
                # Copy down file
                copydowncommand=("scp -i ~/.ssh/%s.pem %s@%s:%s results/" % (ssh_key, sshUser, privateIP, pcapfilename))
                os.system(copydowncommand)

                # Check that copy down of file to the discovery machine is successful 
                filename=("IP%sp%s.pcap" % (privateIP, thisPort))
                file_path=("./results/%s" % filename)
                if os.path.exists(file_path):
                        # If successful, delete remote file
                        print("File %s copied down successfully, will now delete remote file" % pcapfilename)
                        deletefilecommand=("ssh -i ~/.ssh/%s.pem %s@%s 'sudo rm %s'" % (ssh_key, sshUser, privateIP, pcapfilename))
                        os.system(deletefilecommand)
                        print("File %s deleted from remote server" % pcapfilename)

                else:
                        print("Copy down of %s was unsuccessful, please check file exists on remote machine " % pcapfilename)
                        

print("Copy down of .pcap files complete")
