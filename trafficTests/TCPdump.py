#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed


#import requests
import json
import sys
import mysql.connector
import os
import subprocess
import paramiko
import argparse

##################################################################################################

# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())

##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Use procedure to get all private IPs from database and write to list
all_private_IPs=[]
mycursor.callproc('getAllprivateIPs')
for result in mycursor.stored_results():
        records = result.fetchall()
        print("Private IPs are:")
        for item in records:
            all_private_IPs.append(item[0])

print(all_private_IPs)

# Loop through all public IPs of available machines
for privateIP in all_private_IPs: 
    
    print(privateIP)


    # Call procedure to get all open ports for given private IP
    portsbyip = []
    mycursor.callproc('PortsByPrivateIP', [privateIP])
    for result in mycursor.stored_results():
            records = result.fetchall()
            for item in records:
                #print(item[0])
                # Store all open ports for this machine in a list
                portsbyip.append(item[0])
            print(portsbyip)

    # Call procedure to get all RDS ports
    rdsports = []
    mycursor.callproc('getDBports')
    for result in mycursor.stored_results():
            records = result.fetchall()
            for item in records:
                #print(item[0])
                # Store all open ports for this machine in a list
                rdsports.append(item[0])
            print(rdsports)
    
    # Add ports by ip and rds ports list together
    ports = rdsports + portsbyip
    print(ports)
    
    # Write list of ports to file
    with open('ports-%s.txt' % privateIP, 'w') as filehandle:
        for listitem in ports: 
            filehandle.write('%s ' % listitem)
            #filehandle.write('%s\n' % listitem)

    # Call procedure to get machine type for each IP (RHEL/Ubuntu/etc.)
    mycursor.callproc('FlavourByPrivateIP', [privateIP])
    for result in mycursor.stored_results():
            records = result.fetchall()
            for item in records:
                print(item[0])
                machine_type=item[0]
    
    # Set username for SSH dependent upon machine type
    if machine_type == 'rhel' or machine_type == 'amazon':
        ssh_user="ec2-user"
    elif machine_type == 'debian':
         ssh_user="ubuntu"
    else:
        print("Machine type found in database is invalid")
        sys.exit(2)

##################################################################################################  

    # Copy runTCPdump.sh script up onto the machine
    ssh_key=args['ssh_key_name']
    copyupTCPcommand="scp -i ~/.ssh/%s.pem ./runTCPdump.sh ec2-user@%s:~/." % (ssh_key, privateIP)
    #copyupTCPcommand="scp -i final_project.pem ./runTCPdump.sh ec2-user@%s:~/." % (privateIP)
    os.system(copyupTCPcommand)

    # Copy file of ports up onto the machine
    copyupportscommand="scp -i ~/.ssh/%s.pem ./ports-%s.txt ec2-user@%s:~/." % (ssh_key, privateIP, privateIP)
    #copyupportscommand="scp -i final_project.pem ./ports-%s.txt ec2-user@%s:~/." % (privateIP, privateIP)
    os.system(copyupportscommand)

##################################################################################################

    # SSH onto machine and run shell script to start TCPdump
    print("SSHing onto machine") 
    
    cmd="ssh -i " + "~/.ssh/" + ssh_key + ".pem " + ssh_user + "@" + privateIP + " ./runTCPdump.sh " + machine_type + " " + privateIP 
    #cmd="ssh -i ./final_project.pem " + ssh_user + "@" + privateIP + " ./runTCPdump.sh " + machine_type + " " + privateIP 
    os.system(cmd)

    # Remove list of ports .txt file
    os.remove("ports-%s.txt" % privateIP)