#!/bin/bash 

# Shell script to run TCPdump that will be executed on every machine

machine_type=$1
privateIP=$2

##################################################################################################

# Check if tcpdump is installed
if [[ $machine_type == amazon ]] || [[ $machine_type == rhel ]]
then
    sudo yum -y install tcpdump

elif [[ $machine_type == debian ]]
then
    sudo apt-get -y install tcpdump
else 
    echo "Invalid machine type"
    exit 1
fi

##################################################################################################

# Create directory to store .pcap files in if it does not exist
if [[ ! -d /var/log/discovery ]]
then 
    sudo mkdir /var/log/discovery
fi

##################################################################################################

# Get ports from ports file
readarray ports < "ports-$privateIP.txt"

echo "Ports for this instance are $ports"

    for this_port in $ports
    do 
        # Runs command and saves output to file named by IP and port being checked
        filename="/var/log/discovery/IP${privateIP}p${this_port}.pcap"

        
        # Check if the file already exists on that machine
        if [ -f $filename ]
        then
            echo "File ${filename} already exists, please back-up file and remove from this machine using readTCPdump.py script."
            exit 4

        else
            echo "File ${filename} does not already exist"
        
        fi

        # Running tcpdump process in background for a day and writing to file
        echo "Starting tcpdump"
        (nohup sudo tcpdump -G 86400 -W 1 -nn port $this_port -w $filename >/dev/null 2>&1) &
        

        # Get process ID
        pid=$(ps -e | grep tcpdump)  
        echo $pid

        # # Check tcpdump process is running
        # if pgrep -x "tcpdump" > /dev/null
        #     then
        #         # If process is already capturing data, exit from script
        #         echo "TCPdump has started"
              
        #     else
        #         echo "TCPdump failed to start"
        #         exit 5
        #     fi

    done 

    # Exit from the machine
    exit 
