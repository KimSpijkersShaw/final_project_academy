#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed

import os.path 
import os
import json
import sys
import subprocess
import mysql.connector
import paramiko
import pcapkit
import argparse

##################################################################################################
# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())
       
##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Get number of requests to/from each machine, regardless of whether the IP is source or destination
# Need IPS and ports to be identical in each row counted, columns 1&2 and 3&4 to stay together, but these column pairs can switch place
# Count how many rows fulfill these requirements

# Loop through all IP and port combinations

# Use procedure to get all private IPs from database and write to list
all_traffic_IPs=[]
mycursor.callproc('getAllTrafficIPs')
for result in mycursor.stored_results():
        records = result.fetchall()
        #print("Private IPs are:")
        for item in records:
            all_traffic_IPs.append(item[0])

#print(all_private_IPs)


for privateIP in all_traffic_IPs:

    

    # Use procedure to get all ports for each IP that have information in the database.
    all_relevant_ports=[]
    mycursor.callproc('getportswithinfo', [privateIP])
    for result in mycursor.stored_results():
            records = result.fetchall()
            #print("Ports for this IP with information are:")
            for item in records:
                all_relevant_ports.append(item[0])

    

    for port in all_relevant_ports:

        # Use procedure to count no. of rows that contain each IP and related port 
        mycursor.callproc('countTrafficRows', [privateIP, port])
        for result in mycursor.stored_results():
                records = result.fetchall()
                print("No. of rows with IP %s and port %s is " % (privateIP, port))
                for item in records:
                    traffic_count=item[0]

        print(traffic_count)

        # Use procedure to count no. of rows that contain each IP and related port 
        mycursor.callproc('countTrafficRowsSource', [privateIP, port])
        for result in mycursor.stored_results():
                records = result.fetchall()
                print("No. of rows with IP %s and port %s is " % (privateIP, port))
                for item in records:
                    traffic_count_src=item[0]

        print(traffic_count_src)

        # Use procedure to count no. of rows that contain each IP and related port 
        mycursor.callproc('countTrafficRowsDest', [privateIP, port])
        for result in mycursor.stored_results():
                records = result.fetchall()
                print("No. of rows with IP %s and port %s is " % (privateIP, port))
                for item in records:
                    traffic_count_dst=item[0]

        print(traffic_count_dst)

##################################################################################################

        insertsql="INSERT INTO traffic_requests(machine_ip,machine_port,no_of_requests_sent, no_of_requests_rcv,no_of_total_requests) VALUES(%s,%s,%s,%s,%s)"
        sqlvalues=(privateIP, port, traffic_count_src, traffic_count_dst, traffic_count)
        try:
            mycursor.execute(insertsql, sqlvalues)
        except mysql.connector.errors.ProgrammingError:
            print("Bad SQL syntax")
            sys.exit(1)
        try:
            dbconn.commit()
        except:
            print("Insert of traffic request information failed")
            sys.exit(1)

##################################################################################################


