#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed

import os.path 
import os
import json
import sys
import subprocess
import mysql.connector
import paramiko
import pcapkit
import argparse

##################################################################################################

# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())
       
##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Use procedure to get all private IPs from database and write to list
all_traffic_IPs=[]
mycursor.callproc('getAllTrafficIPs')
for result in mycursor.stored_results():
        records = result.fetchall()
        for item in records:
            all_traffic_IPs.append(item[0])


# Loop through all IPs that have traffic stored in the database for them
for privateIP in all_traffic_IPs:

    # Use procedure to get inbound data for this IP
    inbound_traffic=[]
    mycursor.callproc('getInbound', [privateIP])
    for result in mycursor.stored_results():
            records = result.fetchall()
            print("Records are %s" % records)
            for item in records:
                outboundIP=(item[0])
                inboundIP=(item[1])
                totalPackets=(item[2])
                totalData=(item[3])
                print(outboundIP)
                print(inboundIP)
                print(totalPackets)
                print(totalData)

                # Insert data into MySQL database
                insertsql="INSERT INTO inbound_traffic(source_ip,dest_ip,total_packets,total_data) VALUES(%s,%s,%s,%s)"
                sqlvalues=(outboundIP,inboundIP,totalPackets,totalData)
                try:
                    mycursor.execute(insertsql, sqlvalues)
                except mysql.connector.errors.ProgrammingError:
                    print("Bad SQL syntax")
                    sys.exit(1)
                try:
                    dbconn.commit()
                except:
                    print("Insert of traffic data sums information failed")
                    sys.exit(1)

    # Use procedure to get outbound data for this IP
    outbound_traffic=[]
    mycursor.callproc('getOutbound', [privateIP])
    for result in mycursor.stored_results():
            records = result.fetchall()
            print("Records are %s" % records)
            for item in records:
                outboundIP=(item[0])
                inboundIP=(item[1])
                totalPackets=(item[2])
                totalData=(item[3])
                print(outboundIP)
                print(inboundIP)
                print(totalPackets)
                print(totalData)

                # Insert data into MySQL database
                insertsql="INSERT INTO outbound_traffic(source_ip,dest_ip,total_packets,total_data) VALUES(%s,%s,%s,%s)"
                sqlvalues=(outboundIP,inboundIP,totalPackets,totalData)
                try:
                    mycursor.execute(insertsql, sqlvalues)
                except mysql.connector.errors.ProgrammingError:
                    print("Bad SQL syntax")
                    sys.exit(1)
                try:
                    dbconn.commit()
                except:
                    print("Insert of traffic data sums information failed")
                    sys.exit(1)

##################################################################################################
   
# Run mysql procedure to order the inbound and outbound traffic tables
print("Running procedure to order inbound_traffic table")
user=args['dbuser']
password=args['dbpass']
host=args['dbhost']
db=args['dbname']
print(user)
sqlcommand="mysql --user=" + user + " --password=" + password + " --host=" + host + " --database=" + db + " --execute='CALL orderInboundTraffic();'"
os.system(sqlcommand)

