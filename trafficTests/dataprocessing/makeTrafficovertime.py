#!/usr/bin/env python3

# Run script with python3 at beginning
# Need python 3, pip3 installed

import os.path 
import os
import json
import sys
import subprocess
import mysql.connector
import paramiko
import pcapkit
import argparse

##################################################################################################

# Get variables from config file

parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sshk', '--ssh_key_name', default=os.environ.get('SSH_KEY_NAME'), help='The name of the SSH key used to log onto all ec2 instances.')
args = vars(parser.parse_args())
       
##################################################################################################

# Connecting to database server
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print ("Couldn't connect to database")
    sys.exit(1)
print("Connected to database server")

mycursor = dbconn.cursor(dictionary=True)

##################################################################################################

# Use procedure to get all private IPs from database and write to list
all_traffic_IPs=[]
mycursor.callproc('getAllTrafficIPs')
for result in mycursor.stored_results():
        records = result.fetchall()
        for item in records:
            all_traffic_IPs.append(item[0])

# MySQL code
# use all_info;

# set @mintime=(select min(timestamp) from all_traffic);
# set @maxtime=(select max(timestamp) from all_traffic);

# select @mintime, @maxtime, date_add(@mintime, interval 1 second);

# select source_ip, count(source_ip) from all_traffic 
# where str_to_date(timestamp, "%Y-%m-%dT%H:%i:%s.%f") between @mintime and date_add(@mintime, interval 30 second) 
# group by source_ip ;