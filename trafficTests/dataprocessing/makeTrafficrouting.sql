USE all_info;

-- Create table of traffic routing between all IPs
INSERT IGNORE INTO traffic_routing(source_ip, source_port, dest_ip, dest_port)
    SELECT DISTINCT source_ip, source_port, dest_ip, dest_port
    FROM all_traffic;


-- Create filtered table of traffic routing - only show routes between resources within the infrastructure
INSERT IGNORE INTO traffic_routing_filtered(source_ip, source_port, dest_ip, dest_port)
    SELECT source_ip, source_port, dest_ip, dest_port from traffic_routing 
	where (source_ip = "172.31.1.221" or source_ip = "172.31.1.154" or source_ip = "172.31.10.155" or source_ip = "172.31.10.107" or source_ip = "172.31.1.231")
	and (dest_ip = "172.31.1.221" or dest_ip = "172.31.1.154" or dest_ip = "172.31.10.155" or dest_ip = "172.31.10.107" or dest_ip = "172.31.1.231");
    
