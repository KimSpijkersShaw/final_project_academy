#!/bin/bash
user=$1
password=$2
host=$3
db=$4

# Get private IP of the discovery machine
discoveryIP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
echo "Discovery private IP is $discoveryIP"

echo "Calling mysql to delete irrelevant rows"
# Call procedure to remove the rows from all_traffic where IP is the IP of the discovery machine
mysql --user="$user" --password="$password" --host="$host" --database="$db" --execute="CALL deleteDiscovery("$discoveryIP");"
echo "Procedure complete"
