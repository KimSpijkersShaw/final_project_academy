''' Script to use *.csv files from the S3 bucket with pandas to populate the RDS database '''

# Requirements: pip3 install pandas mysql.connector boto3

# Connecting to the RDS Database
# DB info: mysql -h alacademyoct2020-final-project-db.c6jtmnlypkzc.eu-west-1.rds.amazonaws.com -u academy -ppetclinic DB is called all_info

# Connect to the database
#import pymysql
#import sys

import argparse
import sys
import os
import pandas as pd
import numpy as np
import mysql.connector
from mysql.connector import errorcode

# Finding environment variables
parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
#parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
#parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

# Connect to the database
try:
    connection = mysql.connector.connect(host=args['dbhost'],
                                user=args['dbuser'],
                                password=args['dbpass'],
                                database=args['dbname'])
except:
    print("Couldn't connect to database")
    sys.exit(1)

print("Database connection successful")

cursor = connection.cursor()

# Using pandas with the *.csv files to extract only the data we need and input to database

# Iterate through each table
for filename in os.listdir('agent_output_csv'):
    for idname in os.listdir('./agent_output_csv/'+filename):
        df = pd.read_csv (r'./agent_output_csv/'+filename+'/'+idname)

        # Replacing NaN values
        df1 = df.replace(to_replace = np.nan, value ="null")

        # Selecting certain columns from each table
        # Now this is mostly done in athena query instead
        #if filename == 'idmapping':
        #    df1 = df.iloc[:,[2,0,1]]
        #if filename == 'inboundconnection':
        #    df1 = df.iloc[:,[10,1,2,4,5,6,7,8,9]]
        #if filename == 'networkinterface':
        #    df1 = df.iloc[:,[8,1,2,3,4,5,6,7]]
        #if filename == 'osinfo':
        #    df1 = df.iloc[:,[1,2,3,4,5,6]]
        #if filename == 'outboundconnection':
        #    df1 = df.iloc[:,[10,1,2,4,5,6,7,8,9]]
        if filename == 'processes':
            df1 = df1.iloc[:,[7,1,2,4,5]]
        if filename == 'sysperformance':
            df1 = df1.iloc[:,[18,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]]

#       Debugging
#       Fetch all the records
        #sql = "SELECT * FROM `inboundconnection`"
        #cursor.execute(sql)
        #result = cursor.fetchall()
        #for i in result:
#            print(i)
#        break

        # Creating column list for insertion
        COLS = "`,`".join([str(i) for i in df1.columns.tolist()])

        # Insert DataFrame recrds one by one.
        try:
            for i,row in df1.iterrows():
                sql = "INSERT IGNORE INTO `"+filename+ "` (`" +COLS + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
                cursor.execute(sql, tuple(row))
                connection.commit()
            print("Database table updated: "+filename)
        except:
            print("Error - Table not populated this time: "+filename)

connection.close()
cursor.close()
