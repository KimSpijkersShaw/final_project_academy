#!/bin/bash
BUCKET_NAME=$(cat ../config | grep 'bucket_name' | awk '{print $2}')
REGION=$(cat ../config | grep 'region' | awk '{print $2}')

export AWS_DEFAULT_REGION=$REGION

if aws s3api create-bucket --bucket $BUCKET_NAME #\
    # --create-bucket-configuration LocationConstraint=$REGION
then 
    echo "S3 bucket $BUCKET_NAME created successfuly"
else
    echo "S3 bucket creation failed, check the name is unique"
    exit 1
fi 

aws s3api put-public-access-block \
    --bucket $BUCKET_NAME \
    --public-access-block-configuration "BlockPublicAcls=true,\
    IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

aws s3api put-bucket-versioning \
    --bucket $BUCKET_NAME \
    --versioning-configuration Status=Enabled

if aws s3api put-bucket-tagging --bucket $BUCKET_NAME \
    --tagging TagSet='[{Key=Name,Value=final_project},
        {Key=Project,Value=ALAcademy2020Oct},
        {Key=Info,Value=s3-for-key},
        {Key=Owner,Value=ALAcademyOct2020},
        {Key=Start date,Value=7Dec2020},
        {Key=End date,Value=23Dec2020}]'
then 
    echo "Bucket tagged successfully"
else 
    echo "Failed to tag bucket"
    exit 3
fi
