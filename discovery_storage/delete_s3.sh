#!/bin/bash

if ! aws s3 rb s3://$BUCKET_NAME --force
then 
    echo "Failed to delete bucket"
    exit 1
fi