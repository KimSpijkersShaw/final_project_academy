#!/usr/bin/env python
import sys
import os
import argparse
import boto3
import botocore
import mysql.connector
from mysql.connector import errorcode

IGNORE_BUCKETS = ["aws-application-discovery-service-9ij75fjaz5rkv5wvasgjyqvcu",
                  "final-project-alacademyoct2020"]

# Contains all the keys for desired values in the describe RDS JSON
S3_JSON_FIELDS_t = ("Name",)
S3_ACL_JSON_FIELDS_t = ("DisplayName", "Permission")
S3_OBJ_JSON_FIELDS_t = ("Key", "Size")
# Contains all the column names in the s3, s3_acls & s3_objects tables that are read in from JSON
# (total_size is not included as this is calculated in the script)
S3_DATA_FIELDS_t = ("bucket_name", "grantee", "permission", "object", "size")

# Contain column names for each individual table
INSERT_S3_FIELDS = ["bucket_name", "total_size"]
INSERT_S3_ACL_FIELDS = ["bucket_name", "grantee", "permission"]
INSERT_S3_OBJ_FIELDS = ["bucket_name", "object", "size"]

# Contain mysql commands for inserting data into the tables. Does not include the values.
INSERT_S3 = f"INSERT IGNORE INTO s3 ({', '.join(INSERT_S3_FIELDS)}) VALUES "
INSERT_S3_ACL = f"INSERT IGNORE INTO s3_acl ({', '.join(INSERT_S3_ACL_FIELDS)}) VALUES "
INSERT_S3_OBJ = f"INSERT IGNORE INTO s3_objects ({', '.join(INSERT_S3_OBJ_FIELDS)}) VALUES "


def json_extract(obj, key):
    """Recursively fetch values from nested JSON. - Finds the value/s that corresponds to a given
    key in given nested JSON. Returns the value in a list. Modified from a function given here:
    https://hackersandslackers.com/extract-data-from-complex-json-python/
    """
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (list, dict)):
                    if isinstance(v, list) and k == key:
                        for n in range(len(v)):
                            arr.append(v[n])
                    else:
                        extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)

        return arr

    values = extract(obj, arr, key)
    return values


def get_insert_data(data, fields):
    """Returns a tuple containing the values corresponding to the list of keys in *fields*
    in the dictionary *data*
    """
    insert_data_l = []

    for field in fields:
        insert_data_l.append(data[field])

    return tuple(insert_data_l)


def print_list(alist):
    """Prints all the items in a given list"""
    print("PRINT LIST BEGIN")
    for item in alist:
        print(item)
    print("PRINT LIST END")


def handle_data(data_d, fields, cmd_pre):
    """Takes a dictionary, a tuple of keys that correspond with those in the dictionary and part a
    mysql command. Compiles the values in data_d that correspond with the keys in fields in a
    tuple. Appends this tuple to the end of the mysql commands passes it to the insert_data
    function.
    """
    ins_data = get_insert_data(data_d, fields)
    # stores the data for each input to the table
    cmd_data_list = [[] for i in range(1)]

    # iterate through the insert data to create cmd_data/s
    for i, item in enumerate(ins_data):
        if len(item) > 1:
            # Extend to be as long as ins_data[i]
            while len(cmd_data_list) < len(item):
                cmd_data_list.extend([[] for i in range(1)])
                cmd_data_list[-1] = cmd_data_list[-2].copy()

            for m, cmd_data in enumerate(cmd_data_list):
                cmd_data.append(ins_data[i][m])

        else:
            for cmd_data in cmd_data_list:  # append to all cmd_data/s in cmd_data_list
                cmd_data.append(ins_data[i][0])

    for cmd_data in cmd_data_list:  # send all cmd_data in cmd_data_list to insert_data
        tmp_cmd = cmd_pre + f"{tuple(cmd_data)};"
        insert_data(tmp_cmd)


def insert_data(cmd):
    """Takes the string cmd (a mysql command) and executes it on the database with an active
    connection. Script exits if either command execution of database commit fail.
    """
    try:
        curs.execute(cmd)
    except mysql.connector.errors.ProgrammingError:
        print("Insertion failed - Bad SQL syntax. Command listed below.")
        print(cmd)
        sys.exit(1)

    try:
        dbconn.commit()
    except:
        print("Commit failed")
        sys.exit(1)

    print("Mysql command execution and database commit was successful.")


def get_total_size(data_d):
    sizes = data_d['size']

    total_size = 0
    for size in sizes:
        total_size += size

    return total_size


parser = argparse.ArgumentParser(description='Gets information on load balancers using \
  boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'),
                    help='The AWS region in which to search (e.g. us-west-2). Overrides value set \
                      in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'),
                    help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE \
                      environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'),
                    help='The username for accessing the mysql database. Overrides value set in \
                      DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'),
                    help='The password for accessing the mysql database. Overrides value set in \
                      DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'),
                    help='The hostname for the mysql database server. Overrides value set in \
                      DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'),
                    help='The name of the database to use. Overrides value set in DB_NAME \
                      environment variable.')
args = vars(parser.parse_args())

try:  # Connect to the database
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                     host=args['dbhost'],
                                     database=args['dbname'],
                                     password=args['dbpass'])
except:
    print("Couldn't connect to database. See --help for more information on setting \
    database information variables.")
    sys.exit(1)

print("Database connection successful")
curs = dbconn.cursor(dictionary=True)

try:
    session = boto3.Session(
        profile_name=args['profile'], region_name=args['region'])
    s3_client = session.client('s3')
    buckets = (s3_client.list_buckets())['Buckets']

    for bucket in buckets:
        name = bucket['Name']
        if name in IGNORE_BUCKETS:
            continue

        acl = (s3_client.get_bucket_acl(Bucket=name))['Grants']
        objects = (s3_client.list_objects(Bucket=name))

        s3_data_l = []
        s3_acl_data_l = []
        s3_obj_data_l = []

        for s3_field in S3_JSON_FIELDS_t:
            s3_data_l.append(json_extract(bucket, s3_field))

        for s3_acl_field in S3_ACL_JSON_FIELDS_t:
            s3_acl_data_l.append(json_extract(acl, s3_acl_field))

        for s3_obj_field in S3_OBJ_JSON_FIELDS_t:
            s3_obj_data_l.append(json_extract(objects, s3_obj_field))

        s3_data_l += s3_acl_data_l + s3_obj_data_l
        s3_data_d = dict(zip(S3_DATA_FIELDS_t, tuple(s3_data_l)))

        # Calculate the total size of the objects in the bucket and add to the dictionary
        total_size_l = []
        total_size_l.append(get_total_size(s3_data_d))
        s3_data_d.update({'total_size': total_size_l})

        handle_data(s3_data_d, INSERT_S3_FIELDS, INSERT_S3)
        handle_data(s3_data_d, INSERT_S3_ACL_FIELDS, INSERT_S3_ACL)
        handle_data(s3_data_d, INSERT_S3_OBJ_FIELDS, INSERT_S3_OBJ)

    curs.close()
    dbconn.close()

except botocore.exceptions.NoRegionError:
    print('Region not set.  Either run "aws configure" and set it there or use the --region \
        argument.')
