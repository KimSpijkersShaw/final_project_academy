#!/usr/bin/env python

# Script to upload data into database tables:
    # Table 1(ec2): Instances info
    # Table 2(ec2_sg): Instances and their security groups

import json
import pandas as pd
import argparse
import os
import dataframes as dfpy
import db_connections as con

# Gather data for Table 1:
def doInst(dbconn, cursor, df):
    # Create column list
    cols = "`,`".join([str(i) for i in df.columns.tolist()])
    #print("Columns in DB: "+cols)
    # Insert dataframe records
    for i,row in df.iterrows():
        sql = "INSERT IGNORE INTO `ec2` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
        cursor.execute(sql, tuple(row))
        dbconn.commit()
        # print(sql, tuple(row))

# Gather data for Table 2:
def doSecGrp(dbconn, cursor, data):
    sg=data[['instance_id','sec_groups']]
    a=[]
    for x in sg.values:
        for y in x[1]:
            b=dict()
            b={"instance_id": x[0]}
            b.update(y)
            a.append(b)
    df2=pd.DataFrame.from_dict(a)
    df2=df2.rename(columns={'GroupName':'sg_name', 'GroupId':'sg_id'})
    cols = "`,`".join([str(i) for i in df2.columns.tolist()])
    #print("Columns in DB: "+cols)
    for i,row in df2.iterrows():
        sql = "INSERT IGNORE INTO `ec2_sg` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
        cursor.execute(sql, tuple(row))
        dbconn.commit()
        # print(sql, tuple(row))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
    parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
    parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
    parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
    args = vars(parser.parse_args())

    data,df=dfpy.ec2_sgdf()
    dbc,cur=con.dbconnect(args)
    doInst(dbc, cur, df)
    doSecGrp(dbc, cur, data)
    con.dbdisconnect(dbc)
    os.remove("ec2_data.json")
