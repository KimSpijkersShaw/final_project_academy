#!/usr/bin/env python

# Create dataframes for the infrastructure data

import pandas as pd

# DF for Instances 
def ec2_sgdf():
    # Create dataframe
    data = pd.read_json('ec2_data.json')
    df=data.drop(columns='sec_groups')
    return data, df

# DF for Subnets
def subnets_df():
    ds = pd.read_json('subnet_data.json')
    dr = pd.read_json('route_data.json')
    return ds, dr

# DF for Security Groups
def secgrp_df():
    # Create dataframe and convert to string
    din = pd.read_json('sg_ingress_data.json')
    deg = pd.read_json('sg_egress_data.json')
    din['cidr_ips']=din['cidr_ips'].str.join(',')
    din['user_grps']=din['user_grps'].str.join(',')
    deg['cidr_ips']=deg['cidr_ips'].str.join(',')
    deg['user_grps']=deg['user_grps'].str.join(',')
    dsgp = pd.read_json('security_data.json')
    return din, deg, dsgp

# DF for IAM Instance Profile Associations
def iam_df():
    diam = pd.read_json('iam_inst_data.json')
    return diam