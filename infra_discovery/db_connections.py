#!/usr/bin/env python

# Databse connection types for infrastructure data

import sys
import mysql.connector
import pandas as pd

# Create connection to database
def dbconnect(args):
    try:
        dbconn = mysql.connector.connect(host=args['dbhost'],
                                        user=args['dbuser'],
                                        password=args['dbpass'],
                                        database=args['dbname'])
    except:
        print("Could not connect to the database")
        sys.exit(1)
    print("Connected to the database server")
    # Create cursor
    cursor=dbconn.cursor()
    return dbconn, cursor

# def dbconnect_iam():
#     try:
#     dbconn = mysql.connector.connect(host=args['dbhost'],
#                                         user=args['dbuser'],
#                                         password=args['dbpass'],
#                                         database=args['dbname'])
#     except:
#         print("Could not connect to the database")
#         sys.exit(1)
#     print("Connected to the database server")
#     # Create cursor
#     cursor=dbconn.cursor(dictionary=True)
#     return dbconn, cursor

# #dictionary cursor for iam 

# Create databse disconnection
def dbdisconnect(dbconn):
    dbconn.close()