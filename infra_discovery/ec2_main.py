#!/usr/bin/env python

import argparse
import os
import describe_instances
import import_ec2_sg as ies
import describe_subnets
import import_subnets as isb
import describe_sg
import import_sg as isg
import describe_iaminstprof
import import_iam_inst as iam
import dataframes as dfpy
import db_connections as con

parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
parser.add_argument('-sg', '--sgid', default=os.environ.get('DISCOVERY_ID'), help='The name of the security group that is attached to the clients instances. Overrides value set in DISCOVERY_ID environment variable.')
parser.add_argument('-vpc', '--vpcid', default=os.environ.get('CLIENT_VPC_ID'), help='The client VPC ID. Overrides value set in CLIENT_VPC_ID environment variable.')
args = vars(parser.parse_args())

# If user wanted to provide their own arguements they can be overwritten
# args = {
#     "vpcid": "vpc-050dabeaba0efedcf",
#     "region": "eu-west-1",
#     "profile": "academy",
#     "sgid": "sg-07d84922f83012265"
# }

# Get and upload Instances Data
describe_instances.get_data(args)
data,df=dfpy.ec2_sgdf()
dbc,cur=con.dbconnect(args)
ies.doInst(dbc, cur, df)
ies.doSecGrp(dbc, cur, data)
# con.dbdisconnect(dbc)
ies.os.remove("ec2_data.json")

# # Get and upload Subnets Data
describe_subnets.get_subnets(args)
ds,dr=dfpy.subnets_df()
# dbc,cur=con.dbconnect(args)
isb.doSubnets(dbc, cur, ds)
isb.doRoutes(dbc, cur, dr)
# con.dbdisconnect(dbc)
isb.os.remove("subnet_data.json")
isb.os.remove("route_data.json")

# # Get and upload Security Group Data
describe_sg.get_sec(args)
din,deg,dsgp=dfpy.secgrp_df()
# dbc,cur=con.dbconnect(args)
isg.doIngress(dbc, cur, din)
isg.doEgress(dbc, cur, deg)
isg.doSecurity(dbc, cur, dsgp)
# con.dbdisconnect(dbc)
isg.os.remove("sg_ingress_data.json")
isg.os.remove("sg_egress_data.json")
isg.os.remove("security_data.json")

# # Get and upload IAM Instance Profile Data
describe_iaminstprof.get_iam(args)
diam=dfpy.iam_df()
# dbc,cur=con.dbconnect(args)
iam.doIamProfile(dbc, cur, diam)
con.dbdisconnect(dbc)
iam.os.remove("iam_inst_data.json")
