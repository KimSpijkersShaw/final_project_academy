#!/usr/bin/env python

# Script to upload data into database tables:
    # Table 5(sg_ingress_rules): Security Groups Ingress Rules
    # Table 6(sg_egress_rules): Security Groups Egress Rules
    # Table 7(security_groups): sg_id and sg_name (need extra table to get mysql linking)

import json
import pandas as pd
import argparse
import os
import dataframes as dfpy
import db_connections as con

# Gather data for Table 5:
def doIngress(dbconn, cursor, din):
    # Create column list
    cols = "`,`".join([str(i) for i in din.columns.tolist()])
    #print("Columns in DB: "+cols)
    # Insert dataframe records
    for i,row in din.iterrows():
        sql = "INSERT IGNORE INTO `sg_ingress_rules` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
        cursor.execute(sql, tuple(row))
        dbconn.commit()
        # print(sql, tuple(row))

# Gather data for Table 6:
def doEgress(dbconn, cursor, deg):
    # Create column list
    cols = "`,`".join([str(i) for i in deg.columns.tolist()])
    #print("Columns in DB: "+cols)
    # Insert dataframe records
    for i,row in deg.iterrows():
        sql = "INSERT IGNORE INTO `sg_egress_rules` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
        cursor.execute(sql, tuple(row))
        dbconn.commit()
        # print(sql, tuple(row))

def doSecurity(dbconn, cursor, dsgp):
    # Create column list
    cols = "`,`".join([str(i) for i in dsgp.columns.tolist()])
    #print("Columns in DB: "+cols)
    # Insert dataframe records
    for i,row in dsgp.iterrows():
        sql = "INSERT IGNORE INTO `security_groups` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
        cursor.execute(sql, tuple(row))
        dbconn.commit()
        # print(sql, tuple(row))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
    parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
    parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
    parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
    args = vars(parser.parse_args())

    din,deg,dsgp=dfpy.secgrp_df()
    dbc,cur=con.dbconnect(args)
    doIngress(dbc, cur, din)
    doEgress(dbc, cur, deg)
    doSecurity(dbc, cur, dsgp)
    con.dbdisconnect(dbc)
    os.remove("sg_ingress_data.json")
    os.remove("sg_egress_data.json")
    os.remove("security_data.json")
