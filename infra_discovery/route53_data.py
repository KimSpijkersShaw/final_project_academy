#!/usr/bin/env python
import sys
import os
import argparse
import boto3
import json
import botocore
import mysql.connector
from mysql.connector import errorcode

ZONE_ID = "Z07626429N74Z31VDFLI"

# Contains all the keys for desired values in the JSON
ROUTE53_JSON_FIELDS_t = ("Name", "Type", "TTL", "ResourceRecords")
# Contains all the column names in the route_53 table that are read in from JSON
ROUTE53_DATA_FIELDS_t = ("record_name", "type", "ttl", "destination")

# Contain column names for each individual table
INSERT_ROUTE53_FIELDS = ["record_name", "type", "ttl", "destination"]

# Contain mysql commands for inserting data into the tables. Does not include the values.
INSERT_ROUTE53 = f"INSERT IGNORE INTO route_53 ({', '.join(INSERT_ROUTE53_FIELDS)}) VALUES "


def json_extract(obj, key):
    """Recursively fetch values from nested JSON. - Finds the value/s that corresponds to a given
    key in given nested JSON. Returns the value in a list. Modified from a function given here:
    https://hackersandslackers.com/extract-data-from-complex-json-python/
    """
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (list, dict)):
                    if isinstance(v, list) and k == key:
                        for n in range(len(v)):
                            arr.append(v[n])
                    else:
                        extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)

        return arr

    values = extract(obj, arr, key)
    return values


def get_insert_data(data, fields):
    """Returns a tuple containing the values corresponding to the list of keys in *fields*
    in the dictionary *data*
    """
    insert_data_l = []

    for field in fields:
        insert_data_l.append(data[field])

    return tuple(insert_data_l)


def print_list(alist):
    """Prints all the items in a given list"""
    print("PRINT LIST BEGIN")
    for item in alist:
        print(item)
    print("PRINT LIST END")


def handle_data(data_d, fields, cmd_pre):
    """Takes a dictionary, a tuple of keys that correspond with those in the dictionary and part a
    mysql command. Compiles the values in data_d that correspond with the keys in fields in a
    tuple. Appends this tuple to the end of the mysql commands passes it to the insert_data
    function.
    """
    ins_data = get_insert_data(data_d, fields)
    # stores the data for each input to the table
    cmd_data_list = [[] for i in range(1)]

    # iterate through the insert data to create cmd_data/s
    for i, item in enumerate(ins_data):
        if len(item) > 1:
            # Extend to be as long as ins_data[i]
            while len(cmd_data_list) < len(item):
                cmd_data_list.extend([[] for i in range(1)])
                cmd_data_list[-1] = cmd_data_list[-2].copy()

            for m, cmd_data in enumerate(cmd_data_list):
                cmd_data.append(ins_data[i][m])

        else:
            for cmd_data in cmd_data_list:  # append to all cmd_data/s in cmd_data_list
                cmd_data.append(ins_data[i][0])

    for cmd_data in cmd_data_list:  # send all cmd_data in cmd_data_list to insert_data
        tmp_cmd = cmd_pre + f"{tuple(cmd_data)};"
        insert_data(tmp_cmd)


def insert_data(cmd):
    """Takes the string cmd (a mysql command) and executes it on the database with an active
    connection. Script exits if either command execution of database commit fail.
    """
    try:
        curs.execute(cmd)
    except mysql.connector.errors.ProgrammingError:
        print("Insertion failed - Bad SQL syntax. Command listed below.")
        print(cmd)
        sys.exit(1)

    try:
        dbconn.commit()
    except:
        print("Commit failed")
        sys.exit(1)

    print("Mysql command execution and database commit was successful.")


parser = argparse.ArgumentParser(description='Gets information on load balancers using \
  boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'),
                    help='The AWS region in which to search (e.g. us-west-2). Overrides value set \
                      in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'),
                    help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE \
                      environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'),
                    help='The username for accessing the mysql database. Overrides value set in \
                      DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'),
                    help='The password for accessing the mysql database. Overrides value set in \
                      DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'),
                    help='The hostname for the mysql database server. Overrides value set in \
                      DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'),
                    help='The name of the database to use. Overrides value set in DB_NAME \
                      environment variable.')
args = vars(parser.parse_args())

try:  # Connect to the database
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                     host=args['dbhost'],
                                     database=args['dbname'],
                                     password=args['dbpass'])
except:
    print("Couldn't connect to database. See --help for more information on setting \
    database information variables.")
    sys.exit(1)

print("Database connection successful")
curs = dbconn.cursor(dictionary=True)

try:
    curs.execute(
        'SELECT lb_name, dns_name FROM loadbalancers;')
except:
    print("Could not get loadbalancer information from database")
    sys.exit(2)
lb_info = curs.fetchall()

try:
    session = boto3.Session(
        profile_name=args['profile'], region_name=args['region'])
    route53_client = session.client('route53')
    zone = route53_client.get_hosted_zone(Id=ZONE_ID)

    records = (route53_client.list_resource_record_sets(
        HostedZoneId=ZONE_ID))['ResourceRecordSets']

    route53_data_l = []
    for record in records:
        for lb in lb_info:
            for rec in record['ResourceRecords']:
                if lb['dns_name'] in rec['Value']:
                    for route53_field in ROUTE53_JSON_FIELDS_t:
                        route53_data_l.append(
                            json_extract(record, route53_field))

                    route53_data_d = dict(
                        zip(ROUTE53_DATA_FIELDS_t, tuple(route53_data_l)))

                    destination = []
                    destination.append(rec['Value'])
                    route53_data_d['destination'] = destination

                    handle_data(route53_data_d,
                                INSERT_ROUTE53_FIELDS, INSERT_ROUTE53)

    curs.close()
    dbconn.close()

except botocore.exceptions.NoRegionError:
    print('Region not set.  Either run "aws configure" and set it there or use the --region \
        argument.')
