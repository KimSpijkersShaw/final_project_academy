#!/usr/bin/env python3

# Script to get all neccasary information about instances

import json
import boto3
import argparse
import os

def get_data(args):
    filters = [{
        'Name': 'vpc-id',
        'Values': [args['vpcid']]
    }]
    client = boto3.client(service_name='ec2', region_name=args['region'])
    response_inst = client.describe_instances(Filters=filters)
    data=[]
    for r in response_inst['Reservations']:
        for i in r['Instances']:
            skip = 0
            for v in i['BlockDeviceMappings']:
                for b in i['Tags']:
                    if b['Key'] == 'discovery':
                        skip = 1
                        break
                if skip == 1:
                    continue
                try:
                    data.append({"instance_id": i['InstanceId'], "name": [tag['Value'] for tag in i['Tags'] if tag['Key'] == 'Name'][0], "type": i['InstanceType'], "vpc_id": i['VpcId'], "subnet_id": i['SubnetId'], "keyname": i['KeyName'], \
                    "private_dns": i['PrivateDnsName'], "private_ip": i['PrivateIpAddress'], "public_dns": i['PublicDnsName'], "public_ip": i['PublicIpAddress'], "volume_id": v['Ebs']['VolumeId'] , "sec_groups": [grp for grp in i['SecurityGroups'] if grp['GroupId'] != args['sgid']]})
                except:
                    data.append({"instance_id": i['InstanceId'],"name": [tag['Value'] for tag in i['Tags'] if tag['Key'] == 'Name'][0], "type": i['InstanceType'], "vpc_id": i['VpcId'], "subnet_id": i['SubnetId'], "keyname": i['KeyName'], \
                    "private_dns": i['PrivateDnsName'], "private_ip": i['PrivateIpAddress'], "public_dns": "null" , "public_ip": "null", "volume_id": v['Ebs']['VolumeId'], "sec_groups": [grp for grp in i['SecurityGroups'] if grp['GroupId'] != args['sgid']]})

    file = open("ec2_data.json","w")
    file.write(json.dumps(data))
    file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-sg', '--sgid', default=os.environ.get('DISCOVERY_ID'), help='The name of the security group that is attached to the clients instances. Overrides value set in DISCOVERY_ID environment variable.')
    parser.add_argument('-vpc', '--vpcid', default=os.environ.get('CLIENT_VPC_ID'), help='The client VPC ID. Overrides value set in CLIENT_VPC_ID environment variable.')
    args = vars(parser.parse_args())
    get_data(args)
