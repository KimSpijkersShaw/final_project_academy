#!/usr/bin/env python3

# Script to get all neccasary information about security groups

import json
import boto3
import argparse
import os

def get_sec(args):
    filters = [{
        'Name': 'vpc-id',
        'Values': [args['vpcid']]
    }]
    client = boto3.client(service_name='ec2', region_name=args['region'])
    response_sec = client.describe_security_groups(Filters=filters)
    data=[]
    datae=[]
    datasg=[]
    for r in response_sec['SecurityGroups']:
        skip = 0
        try:
            for b in r['Tags']:
                if b['Key'] == 'discovery':
                    skip = 1
                    break
        except:
            pass
        if skip == 1:
            continue
        tmpdatasg={"sg_id": r['GroupId'], "sg_name": r['GroupName']}
        datasg.append(tmpdatasg)
        for p in r['IpPermissions']:
            try:
                if 'FromPort' not in p and 'CidrIp' not in [cidr for cidr in p['IpRanges']]:
                    try:
                        tmpdata={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": p['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in p['IpRanges']], "user_grps": [id['GroupId'] for id in p['UserIdGroupPairs']]}
                        data.append(tmpdata)
                    except:
                        tmpdata={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": p['IpProtocol'], "cidr_ips": "null", "user_grps": [id['GroupId'] for id in p['UserIdGroupPairs']]}
                        data.append(tmpdata)
                elif 'FromPort' not in p:
                    tmpdata={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": p['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in p['IpRanges']], "user_grps": [id['GroupId'] for id in p['UserIdGroupPairs']]}
                    data.append(tmpdata)
                else:
                    tmpdata={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": p['FromPort'], "to_port": p['ToPort'], "ip_protocol": p['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in p['IpRanges']], "user_grps": [id['GroupId'] for id in p['UserIdGroupPairs']]}
                    data.append(tmpdata)
            except:
                pass
        for e in r['IpPermissionsEgress']:
            try:
                if 'FromPort' not in e and 'CidrIp' not in [cidr for cidr in e['IpRanges']]:
                    try:
                        tmpdatae={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": e['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in e['IpRanges']], "user_grps": [id['GroupId'] for id in e['UserIdGroupPairs']]}
                        datae.append(tmpdatae)
                    except:
                        tmpdatae={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": e['IpProtocol'], "cidr_ips": "null", "user_grps": [id['GroupId'] for id in e['UserIdGroupPairs']]}
                        datae.append(tmpdatae)
                elif 'FromPort' not in e:
                    tmpdatae={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": "null", "to_port": "null", "ip_protocol": e['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in e['IpRanges']], "user_grps": [id['GroupId'] for id in e['UserIdGroupPairs']]}
                    datae.append(tmpdatae)
                else:
                    tmpdatae={"sg_id": r['GroupId'], "sg_name": r['GroupName'], "from_port": e['FromPort'], "to_port": e['ToPort'], "ip_protocol": e['IpProtocol'], "cidr_ips": [cidr['CidrIp'] for cidr in e['IpRanges']], "user_grps": [id['GroupId'] for id in e['UserIdGroupPairs']]}
                    datae.append(tmpdatae)
            except:
                pass
            
    file = open("sg_ingress_data.json","w")
    file.write(json.dumps(data))
    file.close()

    file = open("sg_egress_data.json","w")
    file.write(json.dumps(datae))
    file.close()

    file = open("security_data.json","w")
    file.write(json.dumps(datasg))
    file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-vpc', '--vpcid', default=os.environ.get('CLIENT_VPC_ID'), help='The client VPC ID. Overrides value set in CLIENT_VPC_ID environment variable.')
    args = vars(parser.parse_args())
    get_sec(args)