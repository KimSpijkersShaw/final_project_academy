#!/usr/bin/env python
import sys
import os
import argparse
import boto3
import botocore
import mysql.connector
from mysql.connector import errorcode

# Contains all the keys for desired values in the describe load balancers JSON
LB_JSON_FIELDS_t = ("LoadBalancerName", "DNSName", "Protocol", "LoadBalancerPort",
                    "InstancePort", "Subnets", "VPCId", "InstanceId", "SecurityGroups")
# Contains all the column names in the loadbalancer, lb_subnets, lb_sg and lb_instances tables
LB_DATA_FIELDS_t = ("lb_name", "dns_name", "listener_protocol", "listener_lb_port",
                    "listener_instance_port", "subnet_id", "vpc_id", "instance_id", "sg_id")

# Contain column names for each individual table
INSERT_LB_FIELDS = ["lb_name", "dns_name", "listener_protocol", "listener_lb_port",
                    "listener_instance_port", "vpc_id"]
INSERT_LB_SNETS_FIELDS = ["lb_name", "subnet_id"]
INSERT_LB_SG_FIELDS = ["lb_name", "sg_id"]
INSERT_LB_INST_FIELDS = ["lb_name", "instance_id"]

# Contain mysql commands for inserting data into the tables. Does not include the values.
INSERT_LB = f"INSERT IGNORE INTO loadbalancers ({', '.join(INSERT_LB_FIELDS)}) VALUES "
INSERT_LB_SNETS = f"INSERT IGNORE INTO lb_subnets ({', '.join(INSERT_LB_SNETS_FIELDS)}) VALUES "
INSERT_LB_SG = f"INSERT IGNORE INTO lb_sg ({', '.join(INSERT_LB_SG_FIELDS)}) VALUES "
INSERT_LB_INST = f"INSERT IGNORE INTO lb_instances ({', '.join(INSERT_LB_INST_FIELDS)}) VALUES "


def json_extract(obj, key):
    """Recursively fetch values from nested JSON. - Finds the value/s that corresponds to a given
    key in given nested JSON. Returns the value in a list. Modified from a function given here:
    https://hackersandslackers.com/extract-data-from-complex-json-python/
    """
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (list, dict)):
                    if isinstance(v, list) and k == key:
                        for n in range(len(v)):
                            arr.append(v[n])
                    else:
                        extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)

        return arr

    values = extract(obj, arr, key)
    return values


def get_insert_data(data, fields):
    """Returns a tuple containing the values corresponding to the list of keys in *fields*
    in the dictionary *data*
    """
    insert_data_l = []

    for field in fields:
        insert_data_l.append(data[field])

    return tuple(insert_data_l)


def print_list(alist):
    """Prints all the items in a given list"""
    print("PRINT LIST BEGIN")
    for item in alist:
        print(item)
    print("PRINT LIST END")


def handle_data(data_d, fields, cmd_pre):
    """Takes a dictionary, a tuple of keys that correspond with those in the dictionary and part a
    mysql command. Compiles the values in data_d that correspond with the keys in fields in a tuple.
    Appends this tuple to the end of the mysql commands passes it to the insert_data function.
    """
    ins_data = get_insert_data(data_d, fields)
    # stores the data for each input to the table
    cmd_data_list = [[] for i in range(1)]

    # iterate through the insert data to create cmd_data/s
    for i, item in enumerate(ins_data):
        if len(item) > 1:
            # Extend to be as long as ins_data[i]
            while len(cmd_data_list) < len(item):
                cmd_data_list.extend([[] for i in range(1)])
                cmd_data_list[-1] = cmd_data_list[-2].copy()

            for m, cmd_data in enumerate(cmd_data_list):
                cmd_data.append(ins_data[i][m])

        else:
            for cmd_data in cmd_data_list:  # append to all cmd_data/s in cmd_data_list
                cmd_data.append(ins_data[i][0])

    for cmd_data in cmd_data_list:  # send all cmd_data in cmd_data_list to insert_data
        tmp_cmd = cmd_pre + f"{tuple(cmd_data)};"
        insert_data(tmp_cmd)


def insert_data(cmd):
    """Takes the string cmd (a mysql command) and executes it on the database with an active
    connection. Script exits if either command execution of database commit fail.
    """
    try:
        curs.execute(cmd)
    except mysql.connector.errors.ProgrammingError:
        print("Insertion failed - Bad SQL syntax. Command listed below.")
        print(cmd)
        sys.exit(1)

    try:
        dbconn.commit()
    except:
        print("Commit failed")
        sys.exit(1)

    print("Mysql command execution and database commit was successful.")


parser = argparse.ArgumentParser(description='Gets information on load balancers using \
  boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'),
                    help='The AWS region in which to search (e.g. us-west-2). Overrides value set \
                      in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'),
                    help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE \
                      environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'),
                    help='The username for accessing the mysql database. Overrides value set in \
                      DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'),
                    help='The password for accessing the mysql database. Overrides value set in \
                      DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'),
                    help='The hostname for the mysql database server. Overrides value set in \
                      DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'),
                    help='The name of the database to use. Overrides value set in DB_NAME \
                      environment variable.')
args = vars(parser.parse_args())

try:  # Connect to the database
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                     host=args['dbhost'],
                                     database=args['dbname'],
                                     password=args['dbpass'])
except:
    print("Couldn't connect to database. See --help for more information on setting \
    database information variables.")
    sys.exit(1)

print("Database connection successful")
curs = dbconn.cursor(dictionary=True)

try:
    session = boto3.Session(
        profile_name=args['profile'], region_name=args['region'])
    elb_client = session.client('elb')
    response = elb_client.describe_load_balancers()
    lbs = response['LoadBalancerDescriptions']

    for lb in lbs:  # For each loadbalancer found, harvest data and send it to DB
        # print(json.dumps(lb, indent=4, default=str))
        lb_data_l = []
        for lb_field in LB_JSON_FIELDS_t:
            lb_data_l.append(json_extract(lb, lb_field))
        lb_data_d = dict(zip(LB_DATA_FIELDS_t, tuple(lb_data_l)))

        handle_data(lb_data_d, INSERT_LB_FIELDS, INSERT_LB)
        handle_data(lb_data_d, INSERT_LB_SNETS_FIELDS, INSERT_LB_SNETS)
        handle_data(lb_data_d, INSERT_LB_SG_FIELDS, INSERT_LB_SG)
        handle_data(lb_data_d, INSERT_LB_INST_FIELDS, INSERT_LB_INST)

    curs.close()
    dbconn.close()

except botocore.exceptions.NoRegionError:
    print('Region not set.  Either run "aws configure" and set it there or use the \
    --region argument.')
