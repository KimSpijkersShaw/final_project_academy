#!/usr/bin/env python3

# Script to get all neccasary information about subnets and routing tables

import json
import boto3
import re
import argparse
import os

def get_subnets(args):
    filters = [{
        'Name': 'vpc-id',
        'Values': [args['vpcid']]
    }]
    client = boto3.client(service_name='ec2', region_name=args['region'])
    response_subnet = client.describe_subnets(Filters=filters)
    response_route = client.describe_route_tables(Filters=filters)
    data=[]
    datar=[]
    main = 0
    for r in response_subnet['Subnets']:
        tmpdata={"subnet_id": r['SubnetId'], "av_zone": r['AvailabilityZone'], "vpc_id": r['VpcId'],"subnet_name": [tag['Value'] for tag in r['Tags'] if tag['Key'] == 'Name'][0], "cidr_block": r['CidrBlock']}
        for t in response_route['RouteTables']:
            for s in t['Associations']:
                try:
                    if 'SubnetId' not in s and main == 0:
                        tmpdatar={"route_aso": s['RouteTableAssociationId'], "main": s['Main'], "route_tab": s['RouteTableId'], "subnet_id": "null", "gateway_type": "null"}
                        datar.append(tmpdatar)
                        main = 1
                        continue
                    if r['SubnetId'] == s['SubnetId']:
                        if 'SubnetId' in s:
                            tmpdatar={"route_aso": s['RouteTableAssociationId'], "main": s['Main'], "route_tab": s['RouteTableId'], "subnet_id": s['SubnetId']}
                        for u in t['Routes']:
                            if 'GatewayId' in u:
                                if re.search("^igw-", u['GatewayId']):
                                    tmpdata['subnet_type'] = 'Public Subnet'
                            if 'NatGatewayId' in u:
                                tmpdata['subnet_type'] = 'Private Subnet'
                    if 'GatewayId' in u:
                        if re.search("^igw-", u['GatewayId']):
                            tmpdatar['gateway_type'] = u['GatewayId']
                    if 'NatGatewayId' in u:
                        tmpdatar['gateway_type'] = u['NatGatewayId']
                except:
                    pass
        datar.append(tmpdatar)
        tmpdatar=None
        data.append(tmpdata)
        tmpdata=None
        
    file = open("subnet_data.json","w")
    file.write(json.dumps(data))
    file.close()

    file = open("route_data.json","w")
    file.write(json.dumps(datar))
    file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-vpc', '--vpcid', default=os.environ.get('CLIENT_VPC_ID'), help='The client VPC ID. Overrides value set in CLIENT_VPC_ID environment variable.')
    args = vars(parser.parse_args())
    get_subnets(args)