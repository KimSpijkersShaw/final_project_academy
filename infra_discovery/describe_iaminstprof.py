#!/usr/bin/env python3

# Script to get all neccasary information about IAM Instance Profiles

import json
import boto3
import sys
import mysql.connector
import argparse
import os

# Create connection to database
def get_iam(args):
    try:
        dbconn = mysql.connector.connect(host=args['dbhost'],
                                        user=args['dbuser'],
                                        password=args['dbpass'],
                                        database=args['dbname'])
    except:
        print("Could not connect to the database")
        sys.exit(1)
    print("Connected to the database server")
    # Create cursor
    cursor=dbconn.cursor(dictionary=True)

    # Query database for all instance_id's from client
    sql = "SELECT instance_id FROM ec2;"
    cursor.execute(sql)
    data = cursor.fetchall()
    dbconn.close()
    ids =[]
    for line in data:   
        ids.append(line['instance_id'])

    filters = [{
        'Name': 'instance-id',
        'Values': ids
    }]

    client = boto3.client(service_name='ec2', region_name=args['region'])
    response_iam = client.describe_iam_instance_profile_associations(Filters=filters)

    dataiam=[]

    for i in response_iam['IamInstanceProfileAssociations']:
        tmpdata={"assoc_id": i['AssociationId'], "instance_id": i['InstanceId'], "arn": i['IamInstanceProfile']['Arn'], "iam_id": i['IamInstanceProfile']['Id']}
        dataiam.append(tmpdata)

    file = open("iam_inst_data.json","w")
    file.write(json.dumps(dataiam))
    file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gets information on instances using boto3 and puts it into a mysql database. --help for more information.')
    parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
    parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used. Overrides value set in AWS_DEFAULT_PROFILE environment variable.')
    parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
    parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
    parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
    parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
    args = vars(parser.parse_args())
    get_iam(args)