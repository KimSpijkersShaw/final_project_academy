''' Remove the project ssh key locally '''
import os
import argparse

# Command line arguments
parser = argparse.ArgumentParser(description='Remove project key file locally.')
parser.add_argument('-k', '--keyname',
                    default=os.environ.get('SSH_KEY_NAME'),
                    help='The name of the ssh key to be used, without the .pem.')
args = vars(parser.parse_args())

home = os.path.expanduser("~")

os.remove(f"{home}/.ssh/{args['keyname']}.pem")
