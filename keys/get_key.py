''' Download the ssh key needed for the project '''
import os
import sys
import argparse
import stat
import boto3
import botocore

#Command line arguments
parser = argparse.ArgumentParser(description='Get key from S3 bucket.')
parser.add_argument('-r', '--region',
                    default=os.environ.get('AWS_DEFAULT_REGION'),
                    help='The AWS region in which to search (e.g. us-west-2).')
parser.add_argument('-p', '--profile',
                    default=os.environ.get('AWS_DEFAULT_PROFILE'),
                    help='The AWS profile to be used.')
parser.add_argument('-b', '--bucket',
                    default=os.environ.get('BUCKET_NAME'),
                    help='The AWS profile to be used.')
parser.add_argument('-k', '--keyname',
                    default=os.environ.get('SSH_KEY_NAME'),
                    help='The name of the ssh key to be used, without the .pem.')
args = vars(parser.parse_args())

home = os.path.expanduser("~")

# Would add 'if platform is AWS' or 'if key is in s3 bucket' here
s3 = boto3.client('s3')
try:
    s3.download_file(args['bucket'],
                    f"keys/{args['keyname']}.pem",
                    f"{home}/.ssh/{args['keyname']}.pem")
except FileNotFoundError:
    print("Location does not exist")
    sys.exit(1)
except botocore.exceptions.ClientError:
    print("Invalid AWS credentials")
    sys.exit(1)

os.chmod(f"{home}/.ssh/{args['keyname']}.pem", stat.S_IRUSR | stat.S_IWUSR )
