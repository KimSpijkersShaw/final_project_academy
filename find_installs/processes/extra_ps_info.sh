#!/bin/bash
# Get the process running behind each process ID if it exists 
# Useful for finding which java program is running
os=$1
ip=$2
ssh_key_name=$3
process_id=$4

if [[ $os == 'rhel' ]] || [[ $os == 'amazon' ]]
then
    if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip "ps -eo pid,cmd | grep $process_id | grep -v grep | head -1 | awk '{ for(i=2;i<=NF;++i)print \$i }'"
    then 
        echo "Failed to get ps info for process_id $process_id" 1>&2
        exit 1
    fi
elif [[ $os == 'debian' ]]
then 
    if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip "ps -eo pid,cmd | grep $process_id | grep -v grep | head -1 | awk '{ for(i=2;i<=NF;++i)print \$i }'"
    then 
        echo "Failed to get ps info for process_id $process_id" 1>&2
        exit 1
    fi
fi