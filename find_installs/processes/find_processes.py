''' Get information about the process running behind each port and send it to the database '''
import os
import sys
import argparse
import subprocess
import pandas as pd
import mysql.connector

#Command line arguments
parser = argparse.ArgumentParser(description='Get information about the programs installed on each of the machines and send it to the s3 bucket.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2).')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used.')
parser.add_argument('-b', '--bucket', default=os.environ.get('BUCKET_NAME'), help='The AWS profile to be used.')
parser.add_argument('-k', '--keyname', default=os.environ.get('SSH_KEY_NAME'), help='The name of the ssh key to be used, it should be in the path ~/.ssh/KEY_NAME.pem.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())
print(args['dbuser'])
#Make database dbconn
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                host=args['dbhost'],
                                database=args['dbname'],
                                password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)

print("Database dbconn successful")
cursor = dbconn.cursor(dictionary=True, buffered=True)


#Get list of private IPs
try:
    cursor.execute('SELECT instance_id, private_ip, operating_system, os_flavour FROM ec2;')
except:
    print("Could not get machine information from database")
    sys.exit(2)
info = cursor.fetchall()

# For each IP use netstat to get the processes and send to db
for inst in info:
    # Get info
    if 'linux' in inst['operating_system'].lower():
        try:
            os.system(f"./port_processes.sh {inst['os_flavour']} {inst['private_ip']} {args['keyname']}")
        except:
            print(f"Failed to get information about processes for {inst['private_ip']}")
            continue
        if os.stat("processes.txt").st_size == 0:
            print(f"No process information for {inst['private_ip']}")
            continue
        df = pd.read_csv('processes.txt', delim_whitespace=True)
        df[['PID','program_name']] = df['PID/program_name'].str.split('/',expand=True)
        df['instance_id'] = inst['instance_id']
        for pid in df['PID'].tolist():
            proc_cmd = str(subprocess.run(['./extra_ps_info.sh', inst['os_flavour'], inst['private_ip'], args['keyname'], pid], stdout=subprocess.PIPE).stdout.decode().strip())
            if proc_cmd == "":
                proc_cmd = "NULL"
            df.loc[df['PID'] == pid, 'command'] = proc_cmd
        df1 = df.iloc[:,[7, 6, 5, 0, 1, 2, 3, 8]]
    else:
        print(f"Cannot get process information for {inst['private_ip']}, operating system {inst['operating_system']} not supported")
        continue

    # Send to db
    COLS = "`,`".join([str(i) for i in df1.columns.tolist()])
    try:
        for i,row in df1.iterrows():
            sql = f"INSERT IGNORE INTO `port_process_info` (`{COLS}`) VALUES ({'%s,'*(len(row)-1)}%s)"
            cursor.execute(sql, tuple(row))
            dbconn.commit()
    except:
        print(f"Error - Could not add entry for {inst['private_ip']}")
        continue
    print(f"Process information for {inst['private_ip']} added")

    # 

dbconn.close()
cursor.close()

#Clean up
os.remove("processes.txt")