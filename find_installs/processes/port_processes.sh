#!/bin/bash
# Get the process name and process id of the program running behind each port in use on the machine
os=$1
ip=$2
ssh_key_name=$3
echo "protocol local_address foreign_address state PID/program_name" >processes.txt

# If you don't wish to include information about port 111 or 22, add
# | grep -v rpcbind | grep -v sshd 
# to the end of the netstat commands

if [[ $os == 'rhel' ]] || [[ $os == 'amazon' ]]
then
    if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip 'rpm -qa net-tools >/dev/null'
    then 
        ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip 'sudo yum -y install net-tools >/dev/null'
        installed=true
    fi
    ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip 'sudo netstat -tlnp' | tail -n+3 | awk '{print $1,$4,$5,$6,$7}' >>processes.txt
    if [[ $installed == 'true' ]]
    then 
        ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip 'sudo yum -y remove net-tools >/dev/null'
    fi
elif [[ $os == 'debian' ]]
then 
     if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip 'dpkg -l net-tools >/dev/null'
     then
        ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip 'sudo apt-get -y install net-tools >/dev/null'
        installed=true
    fi
    ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip 'sudo netstat -tlnp' | tail -n+3 | awk '{print $1,$4,$5,$6,$7}' >>processes.txt
    if [[ $installed == 'true' ]]
    then
        ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip 'sudo apt-get -y remove net-tools >/dev/null'
    fi
fi