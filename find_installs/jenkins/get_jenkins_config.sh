#!/bin/bash
# Get all of the jenkins jobs folder for a jenkins instance
os=$1
ip=$2
ssh_key_name=$3

if [[ $os == 'rhel' ]] || [[ $os == 'amazon' ]]
then
    user='ec2-user'  
elif [[ $os == 'debian' ]]
then 
    user=ubuntu
fi

mkdir jenkins-info
cd jenkins-info 
if ! scp -r  -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip:/var/lib/jenkins/jobs/* . >/dev/null
then 
    echo "Failed to download jenkins job files" 1>&2
    exit 1
fi
cd ../