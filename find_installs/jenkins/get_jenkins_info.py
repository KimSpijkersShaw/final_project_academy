import os
import xml.etree.ElementTree as ET
import sys
import argparse
import shutil
import json
import pandas as pd
import mysql.connector

#Command line arguments
parser = argparse.ArgumentParser(description='Get information about the programs installed on each of the machines and send it to the s3 bucket.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2).')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used.')
parser.add_argument('-b', '--bucket', default=os.environ.get('BUCKET_NAME'), help='The AWS profile to be used.')
parser.add_argument('-k', '--keyname', default=os.environ.get('SSH_KEY_NAME'), help='The name of the ssh key to be used, it should be in the path ~/.ssh/KEY_NAME.pem.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

# Define XML functions
def get_jenkins_job_basic_info(config_root, inst_id, job_name):
    ''' Gets the basic information about a jenkins job and sends it to the database '''
    all_info = {}
    for elem in config_root:
        if elem.text:
            all_info[elem.tag] = elem.text
        else:
            all_info[elem.tag] = "NULL"
    if all_info['disabled'] == 'true':
        state = 'disabled'
    else:
        state = 'active'
    job_info = {'instance_id' : inst_id, 'job_name' : job_name, 'job_description' : all_info['description'], 'job_state' : state}
    dict_to_db(job_info, 'jenkins_jobs')

def get_parameters(properties, inst_id, job_name):
    ''' Pulls out the parameters in a parameterised jenkins job from the config.xml '''
    for param in properties.iter('hudson.model.StringParameterDefinition'):
        param_info = {item.tag.lower() : item.text for item in param}
        param_info_db = {'instance_id' : inst_id, 'job_name' : job_name,
                        'parameter_name' : param_info['name'],
                        'parameter_description' : param_info['description'],
                        'default_value' : param_info['defaultvalue']}
        dict_to_db(param_info_db, 'jenkins_job_parameters')

def get_scm(scm, inst_id, job_name):
    ''' Pulls out the git repo info for a jenkins job from the config.xml '''
    scm_info = {'instance_id' : inst_id, 'job_name' : job_name}
    scm_info['class'] = scm.get('class')
    scm_info['plugin'] = scm.get('plugin')
    if 'git' in scm_info['class']:
        scm_info['git_repo'] = scm.findall('./userRemoteConfigs/hudson.plugins.git.UserRemoteConfig/url')[0].text
        scm_info['branch'] = scm.findall('./branches/hudson.plugins.git.BranchSpec/name')[0].text
    print(scm_info)
    dict_to_db(scm_info, 'jenkins_jobs_scm')

def get_triggers(triggers, inst_id, job_name):
    ''' Get info on job triggers '''
    for trig in triggers:
        trigger_info = {'instance_id' : inst_id, 'job_name' : job_name}
        if 'ReverseBuildTrigger' in trig.tag:
            trigger_info['trigger_type'] = 'Reverse build trigger'
            trigger_info['upstream_job'] = trig.find('upstreamProjects').text
            trigger_info['trigger_threshold'] = trig.findall('./threshold/name')[0].text
        elif 'SCMTrigger' in trig.tag:
            trigger_info['trigger_type'] = 'scheduled job'
            trigger_info['scheduling'] = trig.find('spec').text
        dict_to_db(trigger_info, 'jenkins_job_triggers')

def get_build_steps(builders, inst_id, job_name):
    ''' Get info on builders from jenkins job config '''
    # To get shell build steps
    for step in builders.iter('hudson.tasks.Shell'):
        step_info = {item.tag.lower() : item.text for item in step}
        step_info_db = {'instance_id' : inst_id, 'job_name' : job_name,
                        'builder_type' : 'shell',
                        'shell_command' : step_info['command']}
        dict_to_db(step_info_db, 'jenkins_job_builders')

# Define DB functions
def insert_data(cmd, values=None, extra=''):
    ''' Insert data into the database with connection cursor using the msql command given as a string '''
    try:
        if values:
            cursor.execute(cmd, values)
        else:
            cursor.execute(cmd)
    except mysql.connector.errors.ProgrammingError:
        print("Insertion failed - Bad SQL syntax. Command listed below.")
        print(cmd)
        sys.exit(1)
    try:
        dbconn.commit()
    except:
        print("Commit failed")
        sys.exit(1)
    print(f"DB commit {extra} successful.")

def dict_to_db(dict, db_table):
    ''' Takes a dictionary and sends it to the database with column headers of the keys '''
    columns = list(dict.keys())
    sqlvalues = list(dict.values())
    insertsql = f"INSERT IGNORE INTO {db_table} ({', '.join(columns)}) VALUES ({'%s,'*(len(columns)-1)}%s)"
    insert_data(insertsql, sqlvalues, f"to {db_table}")


# Define mega function to get all the jenkins info
def gather_jenkins_info(instance_id):
    ''' Collects general job info from the jenkins-info directory, parameters, scm info and build info for each job on a machine that has jenkins '''
    for filename in os.listdir('jenkins-info'):
        tree = ET.parse(f'jenkins-info/{filename}/config.xml')
        root = tree.getroot()
        job_name = filename.lower()
        get_jenkins_job_basic_info(root, instance_id, job_name)
        for child in root:
            if child.tag == 'properties':
                get_parameters(child, instance_id, job_name)
            elif child.tag == 'scm':
                get_scm(child, instance_id, job_name)
            elif child.tag == 'builders':
                get_build_steps(child, instance_id, job_name)
            elif child.tag == 'triggers':
                get_triggers(child, instance_id, job_name)

#Connect to DB
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                host=args['dbhost'],
                                database=args['dbname'],
                                password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)

print("Database dbconn successful")
cursor = dbconn.cursor(dictionary=True, buffered=True)

# Get info about each machine tat has jenkins running on it
query = ("SELECT t.instance_id, private_ip, operating_system, os_flavour, p.command"
        " FROM ec2 as t"
        " JOIN port_process_info as p"
        " ON t.instance_id = p.instance_id"
        " WHERE p.command LIKE '%jenkins%';"
        )
try:
    cursor.execute(query)
except:
    print("Could not get machine information from database")
    sys.exit(2)
info = cursor.fetchall()

#Get all the jenkins info for each relevant instance and send to DB
for inst in info:
    try:
        os.system(f"./get_jenkins_config.sh {inst['os_flavour']} {inst['private_ip']} {args['keyname']}")
    except:
        print(f"Could not download jenkins jobs for {inst['private_ip']}")
        sys.exit(1)
    gather_jenkins_info(inst['private_ip'])
    shutil.rmtree('jenkins-info')

