''' Get information about the programs installed on each of the machines and send it to the s3 bucket '''
import os
import sys
import argparse
import mysql.connector
from put_s3 import send_to_s3

#Command line arguments
parser = argparse.ArgumentParser(description='Get information about the programs installed on each of the machines and send it to the s3 bucket.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2).')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used.')
parser.add_argument('-b', '--bucket', default=os.environ.get('BUCKET_NAME'), help='The AWS profile to be used.')
parser.add_argument('-k', '--keyname', default=os.environ.get('SSH_KEY_NAME'), help='The name of the ssh key to be used, it should be in the path ~/.ssh/KEY_NAME.pem.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())
print(args['dbuser'])
#Make database connection
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                                host=args['dbhost'],
                                database=args['dbname'],
                                password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)

print("Database connection successful")
cur = dbconn.cursor(dictionary=True, buffered=True)


#Get list of private IPs
try:
    cur.execute('SELECT instance_id, private_ip, operating_system, os_flavour FROM ec2;')
except:
    print("Could not get machine information from database")
    sys.exit(2)
info = cur.fetchall()

#For each machine with a linux OS run linux_installs.sh OS-type IP and send output to database
for inst in info:
    if 'linux' in inst['operating_system'].lower():
        try:
            os.system(f"./linux_installs.sh {inst['os_flavour']} {inst['private_ip']} {args['keyname']}")
        except:
            print(f"Failed to get information about installed programs for {inst['private_ip']}")
        send_to_s3(args['bucket'], f"packages-{inst['private_ip']}.txt", f"package-installs/packages-{inst['private_ip']}.txt")
    else:
        print(f"Cannot get program information for {inst['private_ip']}, operating system {inst['operating_system']} not supported")

#Close database connection
cur.close()
dbconn.close()
