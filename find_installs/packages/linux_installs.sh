#!/bin/bash
# Get programs running on linux machines, depending on the operating system
os=$1
ip=$2
ssh_key_name=$3
# bucket_name=$4

if [[ $os == 'rhel' ]] || [[ $os == 'amazon' ]]
then 
    # ssh -i ./$ssh_key_name.pem ec2-user@$ip "rpm -qa --last" >programs.txt
    ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip "rpm -qa" >packages-$ip.txt
elif [[ $os == 'debian' ]]
then 
    # Would give the installed packages since launch of machine and the dates of install
    # sys_date=$(ssh -i ./$ssh_key_name.pem ubuntu@$ip "cat /var/log/dpkg.log | grep 'install ' | head -1 | awk '{print \$1}'")
    # ssh -i ./$ssh_key_name.pem ubuntu@$ip "cat /var/log/dpkg.log | grep 'install ' | grep -v '$sys_date'" >programs.txt 
    # ssh -i ./$ssh_key_name.pem ubuntu@$ip "dpkg -l" >programs.txt
    # To get just package names would do 
    ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip "sudo dpkg-query -f '\${binary:Package}\n' -W" >packages-$ip.txt
else
    echo "Cannot handle operating system $os"
    exit 1
fi

# aws s3 cp packages-$ip.txt s3://$bucket_name/package-installs/packages-$ip.txt