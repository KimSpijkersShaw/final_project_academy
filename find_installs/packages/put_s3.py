''' Function to put a chosen file into a chosen s3 bucket '''
import sys
import boto3

def send_to_s3(bucket, filename, key):
    ''' Copies local item to s3 bucket '''
    s3 = boto3.client('s3')
    try:
        s3.put_object(
            Body = filename,
            Bucket = bucket,
            Key = key,
        )
    except:
        print("File upload failed")
        sys.exit(1)
    print(f"{filename} uploaded to {bucket}/{key}")
