''' Get information about the docker containers on any instances that are running docker '''
import os
import sys
import argparse
import json
import pandas as pd
import mysql.connector


#Command line arguments
parser = argparse.ArgumentParser(description='Get information about the programs installed on each of the machines and send it to the s3 bucket.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2).')
parser.add_argument('-p', '--profile', default=os.environ.get('AWS_DEFAULT_PROFILE'), help='The AWS profile to be used.')
parser.add_argument('-b', '--bucket', default=os.environ.get('BUCKET_NAME'), help='The AWS profile to be used.')
parser.add_argument('-k', '--keyname', default=os.environ.get('SSH_KEY_NAME'), help='The name of the ssh key to be used, it should be in the path ~/.ssh/KEY_NAME.pem.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

# Define functions to connect to database, insert data and query for docker machines
def make_db_connection():
    ''' Make database conection using argparser input credentials dbuser, dbname, dbhost and dbpass '''
    try:
        dbconn = mysql.connector.connect(user=args['dbuser'],
                                    host=args['dbhost'],
                                    database=args['dbname'],
                                    password=args['dbpass'])
    except:
        print("Could not connect to database")
        sys.exit(1)

    print("Database dbconn successful")
    return dbconn

def insert_data(cmd, values=None, extra=''):
    ''' Insert data into the database with connection cursor using the msql command given as a string '''
    try:
        if values:
            cursor.execute(cmd, values)
        else:
            cursor.execute(cmd)
    except mysql.connector.errors.ProgrammingError:
        print("Insertion failed - Bad SQL syntax. Command listed below.")
        print(cmd)
        sys.exit(1)
    try:
        dbconn.commit()
    except:
        print("Commit failed")
        sys.exit(1)
    print(f"Mysql command execution and database commit was successful{extra}.")

def full_ec2_info_process(process_name):
    ''' Get info about only the machines where a chosen process is running '''
    query = ("SELECT t.instance_id, private_ip, operating_system, os_flavour, p.program_name"
            " FROM ec2 as t"
            " JOIN port_process_info as p"
            " ON t.instance_id = p.instance_id"
            f" WHERE p.program_name LIKE '%{process_name}%';"
            )
    try:
        cursor.execute(query)
    except:
        print("Could not get machine information from database")
        sys.exit(2)
    return cursor.fetchall()

JSON_KEYS = ('ID', 'Image', 'Command', 'CreatedAt', 'Status', 'Ports', 'Networks', 'Size', 'LocalVolumes', 'Names', 'Labels')
DOCKER_DATA_FIELDS = ("instance_id", "container_id", "image", "command" ,"created" ,"status" ,"ports", "networks", "size", "local_volumes" ,"names", "labels")

#Get info about only the machines where a docker process is running
dbconn =make_db_connection()
cursor = dbconn.cursor(dictionary=True, buffered=True)
info = full_ec2_info_process('docker')

# For each machine with docker, do a docker ps and send the results to the database
for inst in info:
    # Get info
    if 'linux' in inst['operating_system'].lower():
        try:
            os.system(f"./linux_docker.sh {inst['os_flavour']} {inst['private_ip']} {args['keyname']}")
        except:
            print(f"Failed to get information about docker containers for {inst['private_ip']}")
            continue
        if os.stat("docker.txt").st_size == 0:
            print(f"No docker container information for {inst['private_ip']}")
            continue
        with open('docker.txt') as json_file:
            lines = json_file.readlines()
            containers = []
            for line in lines:
                containers.append(json.loads(line))
    else:
        print(f"Cannot get process information for {inst['private_ip']}, operating system {inst['operating_system']} not supported")
        continue

    for cont in containers:
        sqlvalues=[]
        sqlvalues.append(inst['instance_id'])
        for key in JSON_KEYS:
            if cont[key] == '':
                sqlvalues.append('NULL')
            else:
                sqlvalues.append(cont[key].strip('"'))
        sqlstr = "', '".join(sqlvalues)
        insertsql = f"INSERT IGNORE INTO docker_containers ({', '.join(DOCKER_DATA_FIELDS)}) VALUES ('{sqlstr}')"
        insert_data(insertsql, None, f" for container {cont['Names']} on instance {inst['instance_id']}")

dbconn.close()
cursor.close()

# Clean up
os.remove("docker.txt")