#!/bin/bash
os=$1
ip=$2
ssh_key_name=$3

if [[ $os == 'rhel' ]] || [[ $os == 'amazon' ]]
then 
    if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip "sudo docker container ls --format='{{json .}}'"   >docker.txt
    then 
        if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$ip "docker container ls --format='{{json .}}'"   >docker.txt
        then
            echo "Failed to access docker on instance $ip"
        fi
    fi
elif [[ $os == 'debian' ]]
then 
    if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip "sudo docker container ls --format='{{json .}}'"   >docker.txt
    then 
        if ! ssh -i ~/.ssh/$ssh_key_name.pem -o StrictHostKeyChecking=no ubuntu@$ip "docker container ls --format='{{json .}}'"   >docker.txt
        then
            echo "Failed to access docker on instance $ip"
        fi
    fi
else
    echo "Cannot handle operating system $os"
    exit 1
fi
