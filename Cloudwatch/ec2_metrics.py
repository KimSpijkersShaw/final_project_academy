#!/usr/bin/env python3

import sys
from datetime import datetime, timedelta
import boto3
import os
import mysql.connector
import argparse

# Get variables from config file
parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-r', '--region', default=os.environ.get('AWS_DEFAULT_REGION'), help='The AWS region in which to search (e.g. us-west-2). Overrides value set in AWS_DEFAULT_REGION environment variable.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

region = args['region']

def add_column (column_name):
    """Add a new column for storing metric data for a specific instance"""
    cmd = "mysql -h \`" + args['dbhost'] "\` -u " + args['dbuser'] + " -p" + args['dbpass'] + " -e \" use \`" + args['dbname'] + "\`; alter table cloudwatch_rds add \`" + column_name + "\` VARCHAR(50); \""
    os.system(cmd)

def send_values (data, key, column_name ):
    """Execute SQL command for adding new metric data"""
    for val,time in zip(data, key):
      updatesql = "UPDATE cloudwatch_ec2 SET `" + column_name + "` = %s WHERE timestamp = %s;"
      values = (val,time)
      try:
          cur.execute(updatesql, values)
      except mysql.connector.errors.ProgrammingError:
          print("Bad SQL syntax")
      try:
          dbconn.commit()
      except:
          print("Insert failed")

def send_timestamps (data):
    """Execute SQL command for adding timestamps to the table"""
    for ts in data:
      insertsql = "INSERT IGNORE INTO cloudwatch_ec2(timestamp) VALUES(%s)"
      try:
          cur.execute(insertsql, (ts,))
      except mysql.connector.errors.ProgrammingError:
          print("Bad SQL syntax")
      try:
          dbconn.commit()
      except:
          print("Insert failed")

# Connect to the database
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)
cur = dbconn.cursor(dictionary=True, buffered=True)

# Get IP addresses of ec2 instances and store them in a list
instances = []
cur.execute('SELECT instance_id FROM ec2;')
info = cur.fetchall()
for record in info:
    instances.append(record['instance_id'])

#Get start and end time
current_time = datetime.now()
past_time = current_time - timedelta(days=7)
end = "T".join(str(current_time.replace(second=0, microsecond=0, minute=0)).split())
start = "T".join(str(past_time.replace(second=0, microsecond=0, minute=0)).split())

# Create Boto3 CloudWatch client
cloudwatch = boto3.client('cloudwatch', region_name=region)

# Send API requests for metric data and store the responses in an array
response = []
for instance_id in instances:
  response.append(cloudwatch.get_metric_data(
      MetricDataQueries=[
          {
              'Id': 'ec2CPUUtilization',
              'MetricStat': {
                  'Metric': {
                      'Namespace': 'AWS/EC2',
                      'MetricName': 'CPUUtilization',
                      'Dimensions': [
                          {
                              'Name': 'InstanceId',
                              'Value': instance_id
                          }
                      ]
                  },
                  'Period': 300,
                  'Stat': 'Maximum',
                  'Unit': 'Percent'
              },
              "Label": "ec2CPUUtilization",
              "ReturnData": True
          },
          {
              'Id': 'ec2DiskReadOps',
              'MetricStat': {
                  'Metric': {
                      'Namespace': 'AWS/EC2',
                      'MetricName': 'DiskReadOps',
                      'Dimensions': [
                          {
                              'Name': 'InstanceId',
                              'Value': instance_id
                          }
                      ]
                  },
                  'Period': 300,
                  'Stat': 'Maximum',
                  'Unit': 'Count'
              },
              "Label": "ec2DiskReadOps",
              "ReturnData": True
          },
          {
              'Id': 'ec2DiskWriteOps',
              'MetricStat': {
                  'Metric': {
                      'Namespace': 'AWS/EC2',
                      'MetricName': 'DiskWriteOps',
                      'Dimensions': [
                          {
                              'Name': 'InstanceId',
                              'Value': instance_id
                          }
                      ]
                  },
                  'Period': 300,
                  'Stat': 'Maximum',
                  'Unit': 'Count'
              },
              "Label": "ec2DiskWriteOps",
              "ReturnData": True
          },
          {
              'Id': 'ec2DiskReadBytes',
              'MetricStat': {
                  'Metric': {
                      'Namespace': 'AWS/EC2',
                      'MetricName': 'DiskReadBytes',
                      'Dimensions': [
                          {
                              'Name': 'InstanceId',
                              'Value': instance_id
                          }
                      ]
                  },
                  'Period': 300,
                  'Stat': 'Maximum',
                  'Unit': 'Bytes'
              },
              "Label": "ec2DiskReadBytes",
              "ReturnData": True
          },
          {
              'Id': 'ec2DiskWriteBytes',
              'MetricStat': {
                  'Metric': {
                      'Namespace': 'AWS/EC2',
                      'MetricName': 'DiskWriteBytes',
                      'Dimensions': [
                          {
                              'Name': 'InstanceId',
                              'Value': instance_id
                          }
                      ]
                  },
                  'Period': 300,
                  'Stat': 'Maximum',
                  'Unit': 'Bytes'
              },
              "Label": "ec2DiskWriteBytes",
              "ReturnData": True
          }
      ],
      StartTime=start,
      EndTime=end,
      ScanBy='TimestampDescending',
  ))

# Iterate through the response data and send it to the db
timestamps = []
for instance_reponse,instance_id in zip(response,instances):
    for metric in instance_reponse['MetricDataResults']:
        # Upload timestamps if not done yet
        if not timestamps:
            for date in metric['Timestamps']:
                timestamps.append(str(date).split("+")[0])
            send_timestamps(timestamps)
        column_name = metric['Label'] + "_" + instance_id
        add_column(column_name)
        send_values(metric['Values'], timestamps, column_name)
