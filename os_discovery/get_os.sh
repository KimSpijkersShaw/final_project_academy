#!/bin/bash

# Source config file
cd ../
source ./setenv.sh
cd -

# Grab IP addresses of the ec2 instances and store them in an array
./get_IPs.py > tempfile.txt
readarray IPs < tempfile.txt
rm tempfile.txt

# Iterate through each address to find out the OS used => store it in a file
for address in "${IPs[@]}"
do
      if os=$(ssh -i ~/.ssh/$SSH_KEY_NAME.pem -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
      ec2-user@$address "cat /etc/os-release | grep PRETTY_NAME | awk -F'\"' '{print \$2}'")
      then
        echo "$address $os" >> os_info.txt
      elif os=$(ssh -i ~/.ssh/$SSH_KEY_NAME.pem -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
      ubuntu@$address "cat /etc/os-release | grep PRETTY_NAME | awk -F'\"' '{print \$2}'")
      then
        echo "$address $os" >> os_info.txt
      else
        echo "OS version nad flavour cannot be read (might be issue with SSH access)"
        echo "Exiting..."
        exit 1
      fi
done

# Send the information to the database
./send_os_info.py

# Remove the temporary file
rm os_info.txt
