#!/usr/bin/env python3

import sys
import os
import mysql.connector
import argparse

# Get variables from config file
parser = argparse.ArgumentParser(description='Gets information on load balancers using boto3 and puts it into a mysql database. --help for more information.')
parser.add_argument('-du', '--dbuser', default=os.environ.get('DB_USER'), help='The username for accessing the mysql database. Overrides value set in DB_USER environment variable.')
parser.add_argument('-dp', '--dbpass', default=os.environ.get('DB_PASS'), help='The password for accessing the mysql database. Overrides value set in DB_PASS environment variable.')
parser.add_argument('-dh', '--dbhost', default=os.environ.get('DB_HOST'), help='The hostname for the mysql database server. Overrides value set in DB_HOST environment variable.')
parser.add_argument('-dn', '--dbname', default=os.environ.get('DB_NAME'), help='The name of the database to use. Overrides value set in DB_NAME environment variable.')
args = vars(parser.parse_args())

# Connect to the database
try:
    dbconn = mysql.connector.connect(user=args['dbuser'],
                              host=args['dbhost'],
                              database=args['dbname'],
                              password=args['dbpass'])
except:
    print("Could not connect to database")
    sys.exit(1)

# Define cursor
cur = dbconn.cursor(dictionary=True, buffered=True)

# Open the file containing information regarding OS
file = open("os_info.txt").readlines()

# Read each IP - OS  pair, define flavour and send data to the database
for i in range(int(len(file)/2)):
    address = file[2*i].strip()
    os = file[2*i+1].strip()
    lc_os = os.lower()
    flavour = ""

    if lc_os.find('red hat') or lc_os.find('redhat') or lc_os.find('rhel') or lc_os.find('amazon'):
        flavour = "rhel"
    elif lc_os.find('debian') or lc_os.find('ubuntu'):
        flavour = "debian"
    else:
        print("OS flavour could not be found")
        sys.exit(2)

    updatesql = "UPDATE ec2 SET operating_system = %s, os_flavour = %s WHERE private_ip = %s;"
    values = (os, flavour, address)
    try:
        cur.execute(updatesql, values)
    except mysql.connector.errors.ProgrammingError:
        print("Bad SQL syntax")
    try:
        dbconn.commit()
        print("Success")
    except:
        print("Insert failed")

# Close the connection
cur.close()
dbconn.close()
