output "iam_role_name" {
  value       = aws_iam_role.discovery_role.name
  description = "The name of the IAM role created"
}

output "iam_instance_profile_name" {
  value       = aws_iam_instance_profile.discovery_profile.name
  description = "The name of the IAM instance profile created"
}