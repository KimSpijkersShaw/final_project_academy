#!/bin/bash
region=$(cat ../config | grep 'region' | awk '{print $2}')
bucket_name=$(cat ../config | grep 'bucket_name' | awk '{print $2}')
ssh_key_name=$(cat ../config | grep 'ssh_key_name' | awk '{print $2}')
export AWS_DEFAULT_REGION=$region


#Make a new key pair if it doesn't already exist
checkSSHkey=$(aws ec2 describe-key-pairs --key-name $ssh_key_name --region $region | jq -r '.KeyPairs[].KeyName' 2>/dev/null)
if [[ -z $checkSSHkey ]]
then
    aws ec2 create-key-pair --key-name $ssh_key_name | jq -r '.KeyMaterial' >$ssh_key_name.pem
    chmod 600 $ssh_key_name.pem  
    if aws s3 cp ./$ssh_key_name.pem s3://$bucket_name/keys/${ssh_key_name}.pem
    then
        echo "Created new key pair"
    fi
else 
    if aws s3 cp s3://$bucket_name/keys/${ssh_key_name}.pem ./$ssh_key_name.pem
    then
        chmod 600 $ssh_key_name.pem 
        echo "Using existing key pair $ssh_key_name"
    fi
fi
key_path=$PWD/$ssh_key_name.pem

# Populate dev.tfvars file
./fill_devtfvars.sh

# Check if iam role exists and set count to 0 if it does, 1 if it doesn't
if aws iam list-roles | grep '"RoleName": "discovery_role"' >/dev/null
then 
    export TF_VAR_discovery_profile='discovery_role'
else
    cd make_iam
    terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region" -backend-config="key=statefiles/iam.tfstate"
    terraform apply --var-file=../terrafrom_build/dev.tfvars --auto-approve
    export TF_VAR_discovery_profile=$(terraform output iam_instance_profile_name)
    cd ../
fi

#Run terraform
cd ./terraform_build
export TF_VAR_ssh_key_path=$key_path
terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$region" -backend-config="key=statefiles/discovery.tfstate"
terraform apply --var-file=dev.tfvars --auto-approve

#Run script to propagate security groups to other instances
cd ../propagate_security
./apply_sg.sh

cd ../
# rm $ssh_key_name.pem