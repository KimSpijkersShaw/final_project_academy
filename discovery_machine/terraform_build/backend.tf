terraform {
  backend "s3" {
    key     = "statefiles/discovery.tfstate"
  }
}