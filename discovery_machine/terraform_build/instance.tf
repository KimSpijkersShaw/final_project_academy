resource "aws_instance" "discovery" {
  ami           = var.image_id
  instance_type = var.instance_type
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.discovery_admin.id]
  subnet_id              = var.subnet_id
  iam_instance_profile   = var.discovery_profile
  tags = {
    Name = "discovery-${var.project_name}"
    Project = var.project_name
    Info = "Discovery machine for ${var.project_name}, used to access all of the client's machines."
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}

resource "aws_eip" "discovery" {
  depends_on = [aws_instance.discovery]
  instance = aws_instance.discovery.id
  vpc      = true
  tags = {
    Name = "discovery-${var.project_name}"
    Project = var.project_name
    Info = "Discovery machine for ${var.project_name}, used to access all of the client's machines."
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
    discovery = true
  }
}

# Sleep for a minute to allow discovery instance to start
resource "null_resource" "discoverydelay" {
 depends_on = [aws_eip.discovery]
 provisioner "local-exec" {
   command = "sleep 120"
 }
}

# Add discovery private DNS to ansible inventory/hosts file
resource "null_resource" "add_to_hosts" {
  depends_on = [null_resource.discoverydelay]
  provisioner "local-exec" {
     command = "echo '[discovery]\n${aws_eip.discovery.public_ip}' > ./environments/dev/hosts"
     working_dir = "../ansible_provision"
   }
}

# Provision discovery instance through bastion by calling ansible playbook
resource "null_resource" "provision_discovery" {
  depends_on = [null_resource.add_to_hosts]
  triggers = {
    always_run = timestamp()
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i ./environments/dev site.yml --extra-vars 'key_path=${var.ssh_key_path}'"
    working_dir = "../ansible_provision"
  }
}