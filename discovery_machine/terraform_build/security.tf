resource "aws_security_group" "discovery_admin" {
  name        = "${var.project_name}_discovery_admin"
  description = "Has full access to all machines with the discovery_allow sg, allows ssh from our home IPs."

  vpc_id = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.homeIP
  }

  tags = {
    Name = "discovery_admin_sg"
    Project = var.project_name
    Info = "Security group to apply to discovery machine for ${var.project_name}"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
    discovery = true
  }
}

resource "aws_security_group" "discovery_allow" {
  name        = "${var.project_name}_discovery_allow"
  description = "Allows full access from the dicovery_admin sg."

  vpc_id = var.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    security_groups = [aws_security_group.discovery_admin.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "discovery_allow_sg"
    Project = var.project_name
    Info = "Group to allow full access from discovery machine for ${var.project_name}."
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
    discovery = true
  }
}