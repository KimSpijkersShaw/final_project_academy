variable "region" {
  type    = string
}

variable "vpc_id" {
  type    = string
}

variable "subnet_id" {
  type    = string
}

variable "project_name" {
  type    = string
}

variable "homeIP" {
  type    = list(string)
}

variable "instance_type" {
  type    = string
}

variable "image_id" {
  type    = string
}

variable "ssh_key_name" {
  type    = string
}

variable "team" {
  type = string
}

variable "start_date" {
  type = string
}

variable "end_date" {
  type = string
}

#Needs to be set as an environment variable TF_VAR_ssh_key_path when the key is downloaded
variable "ssh_key_path" {
  type    = string
}

#Set as environment variable by output of iam terraform.
variable "discovery_profile" {
  type    = string
}