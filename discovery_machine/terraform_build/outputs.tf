output "discovery_admin_sg_id" {
  value       = aws_security_group.discovery_admin.id
  description = "ID of discovery admin security group"
}

output "discovery_allow_sg_id" {
  value       = aws_security_group.discovery_allow.id
  description = "ID of discovery allow security group"
}

output "discovery_id" {
  value       = aws_instance.discovery.id
  description = "ID of the discovery instance"
}

output "discovery_public_ip" {
  value       = aws_instance.discovery.public_ip
  description = "The public ip of the discovery"
}

output "discovery_private_ip" {
  value       = aws_instance.discovery.private_ip
  description = "The private ip of the discovery"
}
