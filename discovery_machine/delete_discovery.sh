#!/bin/bash
region=$(cat ../config | grep 'region' | awk '{print $2}')
bucket_name=$(cat ../config | grep 'bucket_name' | awk '{print $2}')
ssh_key_name=$(cat ../config | grep 'ssh_key_name' | awk '{print $2}')
export AWS_DEFAULT_REGION=$region

#Get Key
if aws s3 cp s3://$bucket_name/keys/${ssh_key_name}.pem ./$ssh_key_name.pem
then
    chmod 600 $ssh_key_name.pem 
    echo "Using existing key pair $ssh_key_name"
else 
    echo "Could not download key"
    exit 1
fi
key_path=$PWD/$ssh_key_name.pem
export TF_VAR_ssh_key_path=$key_path

# Populate dev.tfvars file
./fill_devtfvars.sh

#Run script to propagate security groups to other instances
cd ./propagate_security
./remove_sg.sh

#Run terraform
cd ../terraform_build
terraform init
terraform destroy --var-file=dev.tfvars --auto-approve
