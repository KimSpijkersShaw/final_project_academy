#!/bin/bash
client_vpc=$(cat ../../config | grep 'client_vpc_id' | awk '{print $2}')
REGION=$(cat ../config | grep 'region' | awk '{print $2}')

export AWS_DEFAULT_REGION=$REGION

#Can take security group id of discovery allow as first argument if terraform broken
if (( $# < 1 ))
then
    #Get discovery allow security group id
    cd ../terraform_build
    if ! sg_id=$(terraform output discovery_allow_sg_id)
    then 
        echo "Failed to get sg id from terraform output"
        exit 1
    fi
else
    sg_id=$1
    echo "Using discovery_allow sg id: $sg_id"
fi

#Get all relevant AWS resources in the VPC
instances=$(aws ec2 describe-instances --filters Name=vpc-id,Values=$client_vpc | jq -r '.Reservations[].Instances[].InstanceId')
if [[ -z instances ]]
then 
    echo "No ec2 instances in this VPC"
fi 

#Remove the discovery_allow security group from them
for inst in $instances
do 
    #Get current sg IDs
    exist_sg=$(aws ec2 describe-instances --instance-id=$inst | jq -r '.Reservations[].Instances[].NetworkInterfaces[].Groups[].GroupId' | sed -e :a -e '$!N; s/\n/  /; ta')
    if echo $exist_sg | grep $sg_id >/dev/null
    then
        new_sg=$(printf '%s\n' "${exist_sg//$sg_id/}")
        #Apply current ones and new one to each instance if it doesn't have it
        eval aws ec2 modify-instance-attribute --groups $new_sg --instance-id=$inst
        echo "Removed security group from $inst"
    fi
done