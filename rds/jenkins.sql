USE all_info;

CREATE TABLE IF NOT EXISTS `jenkins_jobs` (
  `instance_id` VARCHAR(30),
  `job_name` VARCHAR(50),
  `job_description` VARCHAR(50),
  `job_state` VARCHAR(50),
  PRIMARY KEY (`instance_id`, `job_name`),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id)
);

CREATE TABLE IF NOT EXISTS `jenkins_job_parameters` (
  `instance_id` VARCHAR(30),
  `job_name` VARCHAR(50),
  `parameter_name` VARCHAR(50),
  `parameter_description` VARCHAR(50),
  `default_value` VARCHAR(50),
  PRIMARY KEY (`job_name`, `instance_id`, `parameter_name`),
  FOREIGN KEY (`instance_id`, `job_name`) REFERENCES jenkins_jobs(`instance_id`, `job_name`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `jenkins_jobs_scm` (
  `instance_id` VARCHAR(30),
  `job_name` VARCHAR(50),
  `class` VARCHAR(50),
  `plugin` VARCHAR(50),
  `git_repo` VARCHAR(50),
  `branch` VARCHAR(30),
  PRIMARY KEY (`instance_id`, `job_name`, `class`),
  FOREIGN KEY (`instance_id`, `job_name`) REFERENCES jenkins_jobs(`instance_id`, `job_name`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `jenkins_job_triggers` (
  `instance_id` VARCHAR(30),
  `job_name` VARCHAR(50),
  `trigger_type` VARCHAR(50),
  `upstream_job` VARCHAR(50),
  `trigger_threshold` VARCHAR(50),
  `scheduling` VARCHAR(50),
  PRIMARY KEY (`instance_id`, `job_name`, `trigger_type`),
  FOREIGN KEY (`instance_id`, `job_name`) REFERENCES jenkins_jobs(`instance_id`, `job_name`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS `jenkins_job_builders` (
  `instance_id` VARCHAR(30),
  `job_name` VARCHAR(50),
  `builder_type` VARCHAR(50),
  `shell_command` VARCHAR(1000),
  PRIMARY KEY (`instance_id`, `job_name`, `builder_type`),
  FOREIGN KEY (`instance_id`, `job_name`) REFERENCES jenkins_jobs(`instance_id`, `job_name`)
) ENGINE=INNODB;