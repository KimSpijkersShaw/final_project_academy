CREATE DATABASE IF NOT EXISTS all_info;

USE all_info;

CREATE TABLE IF NOT EXISTS subnets (
  `subnet_id` VARCHAR(20) NOT NULL PRIMARY KEY,
  `av_zone` VARCHAR(10),
  `vpc_id` VARCHAR(100),
  `subnet_name` VARCHAR(100),
  `cidr_block` VARCHAR(20),
  `subnet_type` VARCHAR(100)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS ec2 (
  `instance_id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `name` VARCHAR(100),
  `type` VARCHAR(10),
  `vpc_id` VARCHAR(100),
  `subnet_id` VARCHAR(20),
  `keyname` VARCHAR(100),
  `private_dns` VARCHAR(100),
  `private_ip` VARCHAR(100),
  `public_dns` VARCHAR(100),
  `public_ip` VARCHAR(100),
  `volume_id` VARCHAR(100),
  `operating_system` VARCHAR(100),
  `os_flavour` VARCHAR(100),
  UNIQUE (`private_ip`),
  UNIQUE (`private_dns`),
  FOREIGN KEY (subnet_id) REFERENCES subnets(subnet_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS security_groups (
  `sg_id` VARCHAR(100) NOT NULL PRIMARY KEY,
  `sg_name` VARCHAR(100) NOT NULL
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS ec2_sg (
  `instance_id` VARCHAR(100),
  `sg_id` VARCHAR(100),
  `sg_name` VARCHAR(100),
  PRIMARY KEY (instance_id, sg_id),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id),
  FOREIGN KEY (sg_id) REFERENCES security_groups(sg_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS sg_ingress_rules (
  `sg_id` VARCHAR(100) NOT NULL,
  `sg_name` VARCHAR(100) NOT NULL,
  `from_port` VARCHAR(100),
  `to_port` VARCHAR(100),
  `ip_protocol` VARCHAR(100),
  `cidr_ips` VARCHAR (2048),
  `user_grps` VARCHAR (2048),
  PRIMARY KEY (sg_id, from_port, to_port),
  FOREIGN KEY (sg_id) REFERENCES security_groups(sg_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS sg_egress_rules (
  `sg_id` VARCHAR(100) NOT NULL,
  `sg_name` VARCHAR(100) NOT NULL,
  `from_port` VARCHAR(100),
  `to_port` VARCHAR(100),
  `ip_protocol` VARCHAR(100),
  `cidr_ips` VARCHAR (2048),
  `user_grps` VARCHAR (2048),
  PRIMARY KEY (sg_id, from_port, to_port),
  FOREIGN KEY (sg_id) REFERENCES security_groups(sg_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS loadbalancers (
  `lb_name` VARCHAR(100) NOT NULL PRIMARY KEY,
  `dns_name` VARCHAR(100),
  `listener_protocol` VARCHAR(10),
  `listener_lb_port` VARCHAR(100),
  `listener_instance_port` VARCHAR(100),
  vpc_id VARCHAR(100),
  UNIQUE (`dns_name`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS lb_sg (
  `lb_name` VARCHAR(100),
  `sg_id` VARCHAR(100),
  PRIMARY KEY (lb_name, sg_id),
  FOREIGN KEY (sg_id) REFERENCES security_groups(sg_id),
  FOREIGN KEY (lb_name) REFERENCES loadbalancers(lb_name)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS lb_subnets (
  `lb_name` VARCHAR(100),
  `subnet_id` VARCHAR(20),
  PRIMARY KEY (lb_name, subnet_id),
  FOREIGN KEY (subnet_id) REFERENCES subnets(subnet_id),
  FOREIGN KEY (lb_name) REFERENCES loadbalancers(lb_name)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS lb_instances (
  `lb_name` VARCHAR(100),
  `instance_id` VARCHAR(100),
  PRIMARY KEY (lb_name, instance_id),
  FOREIGN KEY (lb_name) REFERENCES loadbalancers(lb_name),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS routes (
  `route_aso` VARCHAR(100) PRIMARY KEY,
  `main` VARCHAR(100),
  `route_tab` VARCHAR(100),
  `subnet_id` VARCHAR(100),
  `gateway_type` VARCHAR(100),
  FOREIGN KEY (subnet_id) REFERENCES subnets(subnet_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS rds (
  `rds_id` VARCHAR(100) PRIMARY KEY,
  `endpoint` VARCHAR(100),
  `port` VARCHAR(10),
  `vpc_sg_id` VARCHAR(100),
  `db_subnet_group` VARCHAR(100),
  `av_zone` VARCHAR(100),
  `multi_az` VARCHAR(10),
  `db_name` VARCHAR(100),
  `class` VARCHAR(100),
  `engine` VARCHAR(100),
  `engine_version` VARCHAR(100),
  `backup_window` VARCHAR(100),
  `backup_retention` INT(3),
  FOREIGN KEY (vpc_sg_id) REFERENCES security_groups(sg_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS rds_subnets (
  `rds_id` VARCHAR(100),
  `subnet_id` VARCHAR(100),
  PRIMARY KEY (rds_id, subnet_id),
  FOREIGN KEY (rds_id) REFERENCES rds(rds_id),
  FOREIGN KEY (subnet_id) REFERENCES subnets(subnet_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS s3 (
  `bucket_name` VARCHAR(100) PRIMARY KEY,
  `total_size` VARCHAR(100)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS s3_acl (
  `bucket_name` VARCHAR(100),
  `grantee` VARCHAR(100),
  `permission` VARCHAR(100),
  PRIMARY KEY (`bucket_name`,`grantee`,`permission`),
  FOREIGN KEY (bucket_name) REFERENCES s3(bucket_name)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS s3_objects (
  `bucket_name` VARCHAR(100),
  `object` VARCHAR(100),
  `size` VARCHAR(10),
  PRIMARY KEY (`bucket_name`, `object`),
  FOREIGN KEY (bucket_name) REFERENCES s3(bucket_name)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS iam_inst_profile (
  `assoc_id` VARCHAR(100),
  `instance_id` VARCHAR(100),
  `arn` VARCHAR(100),
  `iam_id` VARCHAR(100),
  PRIMARY KEY (`assoc_id`, `instance_id`),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS route_53 (
  `record_name` VARCHAR(100) PRIMARY KEY,
  `type` VARCHAR(100),
  `ttl` VARCHAR(100),
  `destination` VARCHAR(100),
  FOREIGN KEY (destination) REFERENCES loadbalancers(dns_name)
) ENGINE=INNODB;