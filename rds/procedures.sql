
-- DEFINE PROCEDURES 
DELIMITER //

-- Procedure to get all ssh keys
DROP PROCEDURE IF EXISTS getAllsshKeys //
CREATE PROCEDURE getAllsshKeys ()
BEGIN
SELECT key_name FROM ec2;
END //

-- Procedure to get all private IP addresses
DROP PROCEDURE IF EXISTS getAllprivateIPs //
CREATE PROCEDURE getAllprivateIPs ()
BEGIN
SELECT DISTINCT private_ip FROM ec2;
END //

-- Procedure to get ssh key of private IP
DROP PROCEDURE IF EXISTS KeyByPrivateIP //
CREATE PROCEDURE KeyByPrivateIP (PrivateIP varchar(50))
BEGIN
SELECT keyname FROM ec2 WHERE private_IP = PrivateIP;
END //

-- Procedure to get all open ports by private IP address
DROP PROCEDURE IF EXISTS PortsByPrivateIP //
CREATE PROCEDURE PortsByPrivateIP (PrivateIP varchar(50))
BEGIN
SELECT port_number FROM open_ports WHERE private_ip = PrivateIP;
END //

-- Procedure to get machine type by private IP
DROP PROCEDURE IF EXISTS FlavourByPrivateIP //
CREATE PROCEDURE FlavourByPrivateIP (PrivateIP varchar(50))
BEGIN
SELECT os_flavour FROM ec2 WHERE private_ip = PrivateIP;
END //

-- Procedure to get ports of any database
DROP PROCEDURE IF EXISTS getDBports //
CREATE PROCEDURE getDBports () 
BEGIN
SELECT DISTINCT port FROM rds;
END //

-- Procedure to get all ports that have information stored by corresponding IP
DROP PROCEDURE IF EXISTS getportswithinfo //
CREATE PROCEDURE getportswithinfo (sourceIP varchar(50)) 
BEGIN
SELECT DISTINCT source_port FROM all_traffic WHERE source_ip=sourceIP;
END //

-- Procedure to count number of rows that contain the same IP and port regardless of source/dest
DROP PROCEDURE IF EXISTS countTrafficRows //
CREATE PROCEDURE countTrafficRows (IP varchar(50), port int(5)) 
BEGIN
SELECT count(source_ip) FROM all_traffic
WHERE (source_ip=IP AND source_port=port) OR (dest_ip=IP AND dest_port=port);
END //

-- Procedure to count number of rows that contain the same IP and port as source 
DROP PROCEDURE IF EXISTS countTrafficRowsSource //
CREATE PROCEDURE countTrafficRowsSource (IP varchar(50), port int(5)) 
BEGIN
SELECT count(source_ip) FROM all_traffic
WHERE (source_ip=IP AND source_port=port);
END //

-- Procedure to count number of rows that contain the same IP and port as destination
DROP PROCEDURE IF EXISTS countTrafficRowsDest //
CREATE PROCEDURE countTrafficRowsDest (IP varchar(50), port int(5)) 
BEGIN
SELECT count(source_ip) FROM all_traffic
WHERE (dest_ip=IP AND dest_port=port);
END //

-- Procedure to get all traffic IP addresses
DROP PROCEDURE IF EXISTS getAlltrafficIPs //
CREATE PROCEDURE getAlltrafficIPs ()
BEGIN
SELECT DISTINCT source_ip FROM all_traffic;
END //

-- Procedure to get all inbound traffic for a machine with a given IP
DROP PROCEDURE IF EXISTS getInbound //
CREATE PROCEDURE getInbound (privateIP varchar(50))
BEGIN
SELECT dest_ip, source_ip, count(source_ip) AS total_packets, sum(packet_length) AS total_data FROM all_traffic
WHERE dest_ip = privateIP
GROUP BY dest_ip, source_ip 
;
END //

-- Procedure to get all outbound traffic for a machine with a given IP
DROP PROCEDURE IF EXISTS getOutbound //
CREATE PROCEDURE getOutbound (privateIP varchar(50))
BEGIN
SELECT source_ip, dest_ip, count(source_ip) as total_packets, sum(packet_length) as total_data FROM all_traffic
WHERE source_ip = privateIP
GROUP BY source_ip, dest_ip
ORDER BY source_ip, dest_ip;
END //

-- Procedure to delete rows of all_traffic table where IP is discovery machine IP
DROP PROCEDURE IF EXISTS deleteDiscovery //
CREATE PROCEDURE deleteDiscovery (discoveryIP varchar(50))
BEGIN 
DELETE FROM all_traffic WHERE source_ip=discoveryIP OR dest_ip=discoveryIP;
END //

-- Procedure to order the inbound_traffic table
DROP PROCEDURE IF EXISTS orderInboundTraffic //
CREATE PROCEDURE orderInboundTraffic ()
BEGIN 
ALTER TABLE inbound_traffic ORDER BY dest_ip ASC;
END //
 