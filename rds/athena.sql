USE all_info;

CREATE TABLE IF NOT EXISTS osinfo (
  `timestamp` VARCHAR(20),
  `agent_id` VARCHAR(50) PRIMARY KEY,
  `os_name` VARCHAR(50),
  `os_version` VARCHAR(50),
  `cpu_type` VARCHAR(50),
  `host_name` VARCHAR(50),
  `hypervisor` VARCHAR(50),
  FOREIGN KEY (`host_name`) REFERENCES ec2(`private_dns`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS networkinterface (
  timestamp VARCHAR(50),
  `agent_id`VARCHAR(50),
  `name` VARCHAR(50),
  `mac_address` VARCHAR(50),
  `family` VARCHAR(50),
  `ip_address` VARCHAR(50),
  `gateway` VARCHAR(50),
  `net_mask` VARCHAR(50),
  PRIMARY KEY (`agent_id`, `mac_address`),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS sysperformance (
  id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `timestamp` VARCHAR(50),
  `agent_id` VARCHAR(50),
  `total_disk_bytes_read_per_sec_in_kbps` VARCHAR(50),
  `total_disk_bytes_written_per_sec_in_kbps` VARCHAR(50),
  `total_disk_read_ops_per_sec` VARCHAR(50),
  `total_disk_write_ops_per_sec` VARCHAR(50),
  `total_network_bytes_read_per_sec_in_kbps` VARCHAR(50),
  `total_network_bytes_written_per_sec_in_kbps` VARCHAR(50),
  `total_num_logical_processors` VARCHAR(50),
  `total_num_cores` VARCHAR(50),
  `total_num_cpus` VARCHAR(50),
  `total_num_disks` VARCHAR(50),
  `total_num_network_cards` VARCHAR(50),
  `total_cpu_usage_pct` VARCHAR(50),
  `total_disk_size_in_gb` VARCHAR(50),
  `total_disk_free_size_in_gb` VARCHAR(50),
  `total_ram_in_mb` VARCHAR(50),
  `free_ram_in_mb` VARCHAR(50),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS processes (
  id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `agent_provided_timestamp` VARCHAR(50),
  `agent_id` VARCHAR(50),
  `agent_assigned_process_id` VARCHAR(50),
  `name` VARCHAR(50),
  `cmd_line` VARCHAR(50),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS outboundconnection (
  id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `timestamp` VARCHAR(50),
  `agent_id` VARCHAR(50),
  `source_ip` VARCHAR(50),
  `destination_ip` VARCHAR(50),
  `destination_port` VARCHAR(50),
  `ip_version` VARCHAR(50),
  `transport_protocol` VARCHAR(50),
  `agent_assigned_process_id` VARCHAR(50),
  `agent_creation_date` VARCHAR(50),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS inboundconnection (
  id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `timestamp` VARCHAR(50),
  `agent_id` VARCHAR(50),
  `source_ip` VARCHAR(50),
  `destination_ip` VARCHAR(50),
  `destination_port` VARCHAR(50),
  `ip_version` VARCHAR(50),
  `transport_protocol` VARCHAR(50),
  `agent_assigned_process_id` VARCHAR(50),
  `agent_creation_date` VARCHAR(50),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS idmapping (
  `timestamp` VARCHAR(50),
  `agent_id` VARCHAR(50) PRIMARY KEY,
  `server_id` VARCHAR(50),
  FOREIGN KEY (`agent_id`) REFERENCES osinfo(`agent_id`)
) ENGINE=INNODB;