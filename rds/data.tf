data "terraform_remote_state" "discovery" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "statefiles/discovery.tfstate"
    region = var.region
  }
}

