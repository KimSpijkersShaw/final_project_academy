module "db" {
    source  = "terraform-aws-modules/rds/aws"
    version = "~> 2.0"

    identifier = "${var.project_name}-db"

    engine            = "mysql"
    engine_version    = "5.7.19"
    instance_class    = "db.t3.small"
    allocated_storage = 10

    name     = "all_info"
    username = var.user
    password = var.password
    port     = "3306"

    publicly_accessible = true

#   iam_database_authentication_enabled = true

    vpc_security_group_ids = [aws_security_group.discovery_rds.id]
    multi_az = true

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
#   monitoring_interval = "30"
#   monitoring_role_name = "MyRDSMonitoringRole"
#   create_monitoring_role = true

    tags = {
            Name = "rds-${var.project_name}"
            Project = var.project_name
            Info = "Database server for ${var.project_name}"
            Owner = var.team
            Start_date = var.start_date
            End_date = var.end_date
           }

    # DB subnet group
    subnet_ids = var.subnet_ids

    # DB parameter group
    family = "mysql5.7"

    # DB option group
    major_engine_version = "5.7"

    # Snapshot name upon DB deletion
    final_snapshot_identifier = "${var.project_name}-db"

    # Database Deletion Protection
    deletion_protection = false

    parameters = [
    {
        name = "character_set_client"
        value = "utf8"
    },
    {
        name = "character_set_server"
        value = "utf8"
    }
    ]

    options = [
    {
        option_name = "MARIADB_AUDIT_PLUGIN"

        option_settings = [
        {
            name  = "SERVER_AUDIT_EVENTS"
            value = "CONNECT"
        },
        {
            name  = "SERVER_AUDIT_FILE_ROTATIONS"
            value = "37"
        },
        ]
    },
    ]
}

resource "null_resource" "upload_schema" {
  depends_on = [module.db]
  provisioner "local-exec" {
    command = <<_EOF
        scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} *.sql ec2-user@${data.terraform_remote_state.discovery.outputs.discovery_public_ip}:
        ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} ec2-user@${data.terraform_remote_state.discovery.outputs.discovery_public_ip} '
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < infrastructure.sql;
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < gathered.sql;
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < athena.sql;
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < jenkins.sql;
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < procedures.sql;
            rm *.sql'
    _EOF
  }
}
