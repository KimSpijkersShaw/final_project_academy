#!/bin/bash

region=$(cat ../config | grep 'region' | awk '{print $2}')
bucket_name=$(cat ../config | grep 'bucket_name' | awk '{print $2}')
ssh_key_name=$(cat ../config | grep 'ssh_key_name' | awk '{print $2}')
export AWS_DEFAULT_REGION=$region

terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=statefiles/rds.tfstate"
terraform destroy -auto-approve -var-file="dev.tfvars"
