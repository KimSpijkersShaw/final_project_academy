#!/bin/bash

region=$(cat ../config | grep 'region' | awk '{print $2}')
default_vpc=$(cat ../config | grep 'default_vpc_id' | awk '{print $2}')
default_subnets=$(cat ../config | grep 'default_subnets' | awk '{print $2}')
project_name=$(cat ../config | grep 'project_name' | awk '{print $2}')
home_ip=$(cat ../config | grep 'home_ip' | awk '{print $2}')
ssh_key_name=$(cat ../config | grep 'ssh_key_name' | awk '{print $2}')
team=$(cat ../config | grep 'team' | awk '{print $2}')
start_date=$(cat ../config | grep 'start_date' | awk '{print $2}')
end_date=$(cat ../config | grep 'end_date' | awk '{print $2}')
bucket_name=$(cat ../config | grep 'bucket_name' | awk '{print $2}')
dbuser=$(cat ../config | grep 'dbuser' | awk '{print $2}')
dbpass=$(cat ../config | grep 'dbpass' | awk '{print $2}')

sed -e "s/REGION/$region/" \
    -e "s/VPC/$default_vpc/" \
    -e "s|SUBNET|$default_subnets|" \
    -e "s/PROJECT/$project_name/" \
    -e "s|HOMEIP|$home_ip|" \
    -e "s/SSHKEY/$ssh_key_name/" \
    -e "s/TEAM/$team/" \
    -e "s/START/$start_date/" \
    -e "s/END/$end_date/" \
    -e "s/BUCKET/$bucket_name/" \
    -e "s/DBUSER/$dbuser/"\
    -e "s/DBPASS/$dbpass/" dev.tfvars.tmplt >dev.tfvars
