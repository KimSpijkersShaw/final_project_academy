resource "aws_security_group" "discovery_rds" {
  name        = "discovery_rds-${var.project_name}"
  description = "Allows port 3306 access from our home IPs and the discovery machine"

  vpc_id = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = concat(var.homeIP, ["${data.terraform_remote_state.discovery.outputs.discovery_public_ip}/32"])
  }

  tags = {
    Name = "discovery_rds_sg"
    Project = var.project_name
    Info = "Security group to apply to rds for ${var.project_name}"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}
