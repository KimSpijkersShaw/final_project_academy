output "rds_endpoint" {
  value       = module.db.this_db_instance_address
  description = "The endpoint of the rds instance"
}

output "rds_identifier" {
  value       = module.db.this_db_instance_id
  description = "The instance ID of the RDS"
}
