variable "region" {
  type    = string
}

variable "vpc_id" {
  type    = string
}

variable "subnet_ids" {
  type    = list(string)
}

variable "homeIP" {
  type    = list(string)
}

variable "project_name" {
  type = string
}

variable "password" {
  type = string
}

variable "user" {
  type = string
}

variable "team" {
  type = string
}

variable "ssh_key_name" {
  type    = string
}

variable "start_date" {
  type = string
}

variable "end_date" {
  type = string
}

variable "bucket_name" {
  type = string
}

#Needs to be set as an environment variable TF_VAR_ssh_key_path when the key is downloaded
variable "ssh_key_path" {
  type    = string
}