USE all_info;

CREATE TABLE IF NOT EXISTS open_ports (
  `ingress_id` INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `private_ip` VARCHAR(30),
  `protocol` VARCHAR(30),
  `port_number` INT(5),
  `service` VARCHAR(30),
  FOREIGN KEY (`private_ip`) REFERENCES ec2(`private_ip`)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS port_process_info (
  `instance_id` VARCHAR(30),
  `PID` INT(5),
  `program_name` VARCHAR(30),
  `protocol` VARCHAR(30),
  `local_address` VARCHAR(30),
  `foreign_address` VARCHAR(30),
  `state` VARCHAR(30),
  `command` VARCHAR(1000),
  PRIMARY KEY (`instance_id`, `PID`),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS docker_containers (
  `instance_id` VARCHAR(30),
  `container_id` VARCHAR(30),
  `image` VARCHAR(30),
  `command` VARCHAR(30),
  `created` VARCHAR(30),
  `status` VARCHAR(30),
  `ports` VARCHAR(30),
  `networks` VARCHAR(30),
  `size` VARCHAR(30),
  `local_volumes` VARCHAR(30),
  `names` VARCHAR(30),
  `labels` VARCHAR(30),
  PRIMARY KEY (`instance_id`, `container_id`),
  FOREIGN KEY (instance_id) REFERENCES ec2(instance_id)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS all_traffic (
  ingress_id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  source_ip VARCHAR(30),
  source_port INT(5),
  dest_ip VARCHAR(30),
  dest_port INT(5),
  timestamp VARCHAR(50),
  packet_length INT(10),
  packet_id INT(10),
  frag_offset INT(254)
);

CREATE TABLE IF NOT EXISTS traffic_routing(
  ingress_id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  source_ip VARCHAR(30),
  source_port INT(5),
  dest_ip VARCHAR(30),
  dest_port INT(5)
);

CREATE TABLE IF NOT EXISTS traffic_requests(
  ingress_id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  machine_ip VARCHAR(30),
  machine_port INT(5),
  no_of_requests_sent INT(20),
  no_of_requests_rcv INT(20),
  no_of_total_requests INT(20)
);

CREATE TABLE IF NOT EXISTS outbound_traffic(
  source_ip VARCHAR(30),
  dest_ip VARCHAR(30),
  total_packets INT(20),
  total_data INT(20)
);

CREATE TABLE IF NOT EXISTS inbound_traffic(
  dest_ip VARCHAR(30),
  source_ip VARCHAR(30),
  total_packets INT(20),
  total_data INT(20)
);

CREATE TABLE IF NOT EXISTS cloudwatch_rds (
  `timestamp` VARCHAR(30) PRIMARY KEY
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS traffic_routing_filtered(
  ingress_id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  source_ip VARCHAR(30),
  source_port INT(5),
  dest_ip VARCHAR(30),
  dest_port INT(5)
);

